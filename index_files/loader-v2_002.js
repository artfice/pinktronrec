(function() {

    var __hs_cta_json = {"css":"a#cta_button_338227_6fb6df22-65d3-46d3-bd90-730b026252e4 {\n  cursor:pointer !important; \n}\na#cta_button_338227_6fb6df22-65d3-46d3-bd90-730b026252e4:hover {\n}\na#cta_button_338227_6fb6df22-65d3-46d3-bd90-730b026252e4:active, #cta_button_338227_6fb6df22-65d3-46d3-bd90-730b026252e4:active:hover {\n}\n\n","image":false,"_id":"false","raw_html":"<a id=\"cta_button_338227_6fb6df22-65d3-46d3-bd90-730b026252e4\" class=\"cta_button \" href=\"//cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=6fb6df22-65d3-46d3-bd90-730b026252e4&placement_guid=dac20154-ad70-4ac9-bb6c-700d95003b51&portal_id=338227&redirect_url=APefjpFdkv0VnaYUnHOHDNTLvOtaQFgMirGnFbMJgFu_YjDhUieKWm-3z9bn7epywzyDDjNC5k4nwGHjDz0Ut8TWQKyHtplFF1D4krbs6iroW0XMNdc68hgCPTQKpCyhPg7SLEs-8BlcqcMa-iRbjAHqCauaS1n5rQ&hsutk=&canon=http%3A%2F%2Fwww.californiafamilyfitness.com%2F\"  style=\"/*hs-extra-styles*/\" cta_dest_link=\"http://www.californiafamilyfitness.com/gyms/gym-finder\" title=\"FIND A GYM NEAR YOU\"> FIND A GYM NEAR YOU </a> ","image_html":"<a id=\"cta_button_338227_6fb6df22-65d3-46d3-bd90-730b026252e4\" class=\"cta_button\" href=\"//cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=6fb6df22-65d3-46d3-bd90-730b026252e4&placement_guid=dac20154-ad70-4ac9-bb6c-700d95003b51&portal_id=338227&redirect_url=APefjpFdkv0VnaYUnHOHDNTLvOtaQFgMirGnFbMJgFu_YjDhUieKWm-3z9bn7epywzyDDjNC5k4nwGHjDz0Ut8TWQKyHtplFF1D4krbs6iroW0XMNdc68hgCPTQKpCyhPg7SLEs-8BlcqcMa-iRbjAHqCauaS1n5rQ&hsutk=&canon=http%3A%2F%2Fwww.californiafamilyfitness.com%2F\"  cta_dest_link=\"http://www.californiafamilyfitness.com/gyms/gym-finder\"> <img id=\"hs-cta-img-dac20154-ad70-4ac9-bb6c-700d95003b51\" class=\"hs-cta-img \" style=\"border-width: 0px; /*hs-extra-styles*/\" mce_noresize=\"1\" alt=\"FIND A GYM NEAR YOU\" src=\"http://cdn2.hubspot.net/hubshot/17/01/02/bed0833e-9098-44e4-bd53-3129cb2d929f.png\" /> </a> ","is_image":false,"_class":"hs-cta-dac20154-ad70-4ac9-bb6c-700d95003b51"};
    var __hs_cta = {};

    __hs_cta.drop = function() {
        var is_legacy = document.getElementById('hs-cta-ie-element') && true || false,
            html = __hs_cta_json.image_html,
            tags = __hs_cta.getTags(),
            is_image = __hs_cta_json.is_image,
            parent, _style;

        if (!is_legacy && !is_image) {
            parent = (document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]);

            _style = document.createElement('style');
            parent.insertBefore(_style, parent.childNodes[0]);
            try {
                default_css = ".hs-cta-wrapper p, .hs-cta-wrapper div { margin: 0; padding: 0; }";
                cta_css = default_css + " " + __hs_cta_json.css;
                // http://blog.coderlab.us/2005/09/22/using-the-innertext-property-with-firefox/
                _style[document.all ? 'innerText' : 'textContent'] = cta_css;
            } catch (e) {
                // addressing an ie9 issue
                _style.styleSheet.cssText = cta_css;
            }

            html = __hs_cta_json.raw_html;
        }

        for (var i = 0; i < tags.length; ++i) {

            var tag = tags[i];
            var images = tag.getElementsByTagName('img');
            var cssText = "";
            var align = "";
            for (var j = 0; j < images.length; j++) {
                align = images[j].align;
                images[j].style.border = '';
                images[j].style.display = '';
                cssText = images[j].style.cssText;
            }

            if (align == "right") {
                tag.style.display = "block";
                tag.style.textAlign = "right";
            } else if (align == "middle") {
                tag.style.display = "block";
                tag.style.textAlign = "center";
            }

            tag.innerHTML = html.replace('/*hs-extra-styles*/', cssText);
            tag.style.visibility = 'visible';
            tag.setAttribute('data-hs-drop', 'true');
            window.hbspt && hbspt.cta && hbspt.cta.afterLoad && hbspt.cta.afterLoad();
        }

        return tags;
    };

    __hs_cta.getTags = function() {
        var allTags, check, i, divTags, spanTags,
            tags = [];
            if (document.getElementsByClassName) {
                allTags = document.getElementsByClassName(__hs_cta_json._class);
                check = function(ele) {
                    return (ele.nodeName == 'DIV' || ele.nodeName == 'SPAN') && (!ele.getAttribute('data-hs-drop'));
                };
            } else {
                allTags = [];
                divTags = document.getElementsByTagName("div");
                spanTags = document.getElementsByTagName("span");
                for (i = 0; i < spanTags.length; i++) {
                    allTags.push(spanTags[i]);
                }
                for (i = 0; i < divTags.length; i++) {
                    allTags.push(divTags[i]);
                }

                check = function(ele) {
                    return (ele.className.indexOf(__hs_cta_json._class) > -1) && (!ele.getAttribute('data-hs-drop'));
                };
            }
            for (i = 0; i < allTags.length; ++i) {
                if (check(allTags[i])) {
                    tags.push(allTags[i]);
                }
            }
        return tags;
    };

    // need to do a slight timeout so IE has time to react
    setTimeout(__hs_cta.drop, 10);
}());
