$(function () {
    "use strict";
    $("html")
        .removeClass("no-js")
        .addClass("js");

    var body =              $("body"),
        header =            $(".site-header:not(.site-header--simple)"),
        utility =           $(".utility"),
        // numbers
        header_height =     80,
        scrolled_height =   60,
        utility_height =    40,
        window_width =      $(window).width(),
        window_height =     $(window).height(),
        doc_height =        $(document).height(),
        // misc
        instagram_feed,
        // functions
        // returns true if we're in an iframe, to detect if this is an editor window
        is_editor = function () {
            try {
                return window.self !== window.top;
            } catch (e) {
                return true;
            }
        },
        // initializes the mobile menu
        menu_init = function () {
            var menu = header.find(".site-header__menu .hs-menu-wrapper"),
                menu_active = false,
                menu_toggle = $("<button class='site-header__control--toggle site-header__control'>Menu</button>"),
                menu_close = $("<button class='site-header__control--close site-header__control'>Close</button>"),
                speed = "fast",
                open_menu = function () {
                    menu_active = true;
                    header.addClass("site-header--menu-open");
                    menu.slideDown(speed);
                },
                close_menu = function () {
                    if (menu_active) {
                        menu_active = false;
                        header.removeClass("site-header--menu-open");
                        menu.slideUp(speed);
                    }
                },
                toggle_menu = function () {
                    if (!menu_active) {
                        open_menu();
                    } else {
                        close_menu();
                    }
                },
                children_init = function () {
                    var children = menu.find(".hs-menu-depth-1 > .hs-menu-children-wrapper"),
                        parents = menu.find(".hs-item-has-children");
                    if (children.length > 0) {
                        children.each(function () {
                            var items = $(this).find(".hs-menu-depth-2"),
                                parent = $(this).closest(".hs-menu-children-wrapper"),
                                total = items.length,
                                group_size = Math.ceil(total / 2),
                                group_1, group_2;
                            if (total >= 10) {
                                group_1 = items.slice(0, group_size);
                                group_2 = items.slice(group_size);
                                group_1.wrapAll("<div class='hs-menu-children-column'></div>");
                                group_2.wrapAll("<div class='hs-menu-children-column'></div>");
                                parent.addClass("hs-menu-children-wrapper--split");
                            }
                        });
                        parents.each(function () {
                            var child_toggle = $("<button class='site-header__control site-header__control--child-toggle'>Open</button>"),
                                parent = $(this),
                                child = parent.find(".hs-menu-children-wrapper");
                            parent.prepend(child_toggle);
                            child_toggle.click(function () {
                                parent.toggleClass("hs-item-has-children--active");
                                child.slideToggle();
                            });
                        });
                    }
                };
            if (menu.length > 0) {
                $(".site-header__logo").append(menu_toggle);
                menu.append(menu_close);
                menu_toggle.click(toggle_menu);
                menu_close.click(close_menu);
                children_init();
                $(document)
                    .on("click touchend", function (e) {
                        if ($(e.target).closest(menu.add(menu_toggle)).length < 1) {
                            close_menu();
                        }
                    });
            }
        },
        select_nav_init = function () {
            $(".select-nav").each(function () {
                var menu = $(this),
                    nav = menu.find("ul"),
                    items = nav.find("li"),
                    links = nav.find("a"),
                    go = menu.find(".select-nav__go"),
                    open = false;

                nav.on("click", function () {
                    open = !open;
                    menu.toggleClass("select-nav--active", open);
                    parallax_refresh();
                });
                links.on("click", function (e) {
                    e.preventDefault();
                    var href = $(this).attr("href");
                    if (href !== document.location.pathname) {
                        go.attr({
                            href: href,
                            target: $(this).attr("target")
                        });
                    } else {
                        go.attr({
                            href: "#",
                            target: ""
                        });
                    }
                    items.removeClass("select-nav__item--active");
                    $(this).closest(items).addClass("select-nav__item--active");
                });
                $(document).on("click touchend", function (e) {
                    if ($(e.target).closest(menu).length < 1) {
                        menu.removeClass("select-nav--active");
                        open = false;
                        $(window).trigger("resize");
                    }
                });
            });
        },
        sticky_footer_init = function () {
            var sticky_footer = $(".sticky-footer"),
                close;
            if (sticky_footer.length > 0) {
                close = $("<button class='sticky-footer__close'>Close</button>");
                sticky_footer.append(close);
                close.click(function (e) {
                    e.stopPropagation();
                    sticky_footer
                        .addClass("sticky-footer--closed");
                    document.cookie = "sticky_footer_close=1";
                });
                sticky_footer.click(function () {
                    if (sticky_footer.is(".sticky-footer--closed")) {
                        sticky_footer
                            .removeClass("sticky-footer--closed")
                            .find(".hs-input")
                            .first()
                            .focus();
                        document.cookie = "sticky_footer_close=0";
                    }
                });
            }
        },
        back_to_top_init = function () {
            if ($(document).height() >= ($(window).height() * 1.5)) {
                var to_top = $("<a href='#' class='back-to-top' title='Back to Top'>Back to Top</a>");
                body.append(to_top);
                to_top.click(function (e) {
                    e.preventDefault();
                    $("body").data("scrolling", true);
                    $('html, body').animate({
                        scrollTop: 0
                    }, 1000,
                    function () {
                        $("body").data("scrolling", false);
                    });
                });
                body.waypoint(function (direction) {
                    to_top.toggleClass("back-to-top--active", direction === "down");
                }, { offset: function () {
                    return -$(window).height() * 1.5;
                }});
            }
        },
        // sizes the page-banner on the home page to fill the viewport and always have enough space for the body
        home_banner_init = function () {
            if (body.is(".home")) {
                var banner = $(".top-banner .page-banner"),
                    banner_body = banner.find(".page-banner__body"),
                    get_height = function () {
                        var height = $(window).height() - utility.outerHeight();
                        return header.css("position") === "fixed" || header.css("position") === "absolute" ? height : height - header.outerHeight();
                    },
                    get_min_height = function () {
                        var height;
                        banner_body
                            .css("height", "")
                            .removeClass("measured");
                        height = banner_body.outerHeight();
                        banner_body
                            .css("height", height)
                            .addClass("measured");
                        return height + header.outerHeight();
                    },
                    set_heights = function () {
                        banner.height(get_height());
                        banner.css("min-height", get_min_height());
                    };
                set_heights();
                $(window).on("width_change load", set_heights);
            }
        },
        // initializes the custom social sharing icons
        social_sharing_init = function () {
            var sharing = $(".social-media--sharing"),
                links, popup;
            if (sharing.length > 0) {
                popup = function (url, width, height) {
                    var dual_left = window.screenLeft !== undefined ? window.screenLeft : screen.left,
                        dual_top = window.screenTop !== undefined ? window.screenTop : screen.top,
                        screen_width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width,
                        screen_height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height,
                        left = (screen_width / 2) - (width / 2) + dual_left,
                        top = (screen_height /2) - (height / 2) + dual_top,
                        new_window = window.open(
                            url,
                            "",
                            "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=" + width + ",height=" + height + ",top=" + top + ",left=" + left
                            );
                    if (new_window.focus) {
                        new_window.focus();
                    }
                };

                links = sharing.find(".social-media__service");

                sharing.find(".social-media__service").click(function (e) {
                    e.preventDefault();
                    popup($(this).attr("href"), 500, 300);
                });

                if (sharing.is(".social-media--sharing--counts")) {
                    links.each(function () {
                        var link = $(this),
                            service = link.data("service"),
                            url = sharing.data("url"),
                            api_url, key, api_data;
                        switch (service) {
                            case "facebook":
                                api_url = "http://graph.facebook.com/?id=" + url;
                                key = "shares";
                                break;
                            case "twitter":
                                api_url = "http://cdn.api.twitter.com/1/urls/count.json?url=" + url + "&callback=?";
                                key = "count";
                                break;
                            case "pinterest":
                                api_url = "http://api.pinterest.com/v1/urls/count.json?url=" + url + "&callback=?";
                                key = "count";
                                break;
                            case "googleplus":
                                api_url = "https://clients6.google.com/rpc";
                                api_data = {
                                    "method": "pos.plusones.get",
                                    "id": url,
                                    "params": {
                                        "nolog": true,
                                        "id": url,
                                        "source": "widget",
                                        "userId": "@viewer",
                                        "groupId": "@self"
                                    },
                                    "jsonrpc": "2.0",
                                    "key": "p",
                                    "apiVersion": "v1"
                                };
                                break;
                        }

                        if (service === "googleplus") {
                            $.ajax({
                                type: "POST",
                                url: api_url,
                                processData: true,
                                contentType: "application/json",
                                data: JSON.stringify(api_data),
                                success: function (data) {
                                    var count = data.result.metadata.globalCounts.count || 0;
                                    if (count.toString().length > 0) {
                                        link
                                            .addClass("social-media__service--has-count")
                                            .after("<span class='social-media__count'>" + count + "</span>");
                                    }
                                }
                            });
                        } else {
                            $.getJSON(api_url, function (data) {
                                var count = data[key] || 0;
                                if (count.toString().length > 0) {
                                    link
                                        .addClass("social-media__service--has-count")
                                        .after("<span class='social-media__count'>" + count + "</span>");
                                }
                            });
                        }
                    });
                }
            }
        },
        parallax_refresh = function () {
            $(window)
                .trigger("resize.px.parallax")
                .trigger("scroll");
        },
        doc_height_refresh = function () {
            var new_height = $(document).height();
            if (new_height !== doc_height) {
                doc_height = new_height;
                $(window).trigger("doc_height_change");
                parallax_refresh();
            }
        },
        subscribe_form_init = function () {
            var forms = $(".hs_cos_wrapper_type_blog_subscribe"),
                active_class = "field-active";
            if (forms.length > 0) {
                forms.each(function () {
                    var form = $(this),
                        frequency = form.find("[class*=_subscription]"),
                        frequency_label = frequency.children("label"),
                        inputs = frequency.find(".hs-input");
                    frequency_label.click(function () {
                        frequency.toggleClass(active_class);
                    });
                    inputs.change(function () {
                        frequency.removeClass(active_class);
                    });
                    $(document)
                        .click(function (e) {
                            if ($(e.target).closest(frequency).length < 1) {
                                frequency.removeClass(active_class);
                            }
                        });
                });
            }
        },
        // Adds some special classes to the persona page menu
        persona_menu_init = function () {
            var menu = $(".persona-menu"),
                items;
            if (menu.length > 0) {
                items = menu.find(".hs-menu-item");
                items
                    .addClass("persona-menu__item")
                    .each(function () {
                        $(this)
                            .addClass(
                                "persona-menu__item--" +
                                $(this)
                                    .text()
                                    .toLowerCase()
                                    .replace("&", "and")
                                    .replace(/\s/g, "-")
                            )
                            .find("a")
                            .wrapInner("<span class='persona-menu__link'></span>");
                    });
                items.equal_height(false);
            }
        },
        // special functions for landing pages
        landing_page_init = function () {
            if (body.is(".landing-page--offer")) {
                var sidebar = $(".sidebar"),
                    main_content = $(".main-content"),
                    parallax = $(".parallax-window"),
                    set_dimensions = function () {
                        var width = sidebar[0].getBoundingClientRect().left,
                            height = main_content.outerHeight();
                        if (sidebar.css("float") === "left") {
                            parallax
                                .css("height", "")
                                .width(width);
                        } else {
                            parallax
                                .css("width", "")
                                .height(height);
                        }
                    };
                set_dimensions();
                $(window).on("load width_change", set_dimensions);
            }
        },
        // adjust hash-scrolling on load for the fixed header
        scroll_to_hash = function () {
            if (location.hash && header.css("position") === "fixed") {
                var target = $(location.hash),
                    position;

                target = target.length ? target : $('[name=' + location.hash.slice(1) + ']');
                if (target.length) {
                    position = target.offset().top - header_height;
                    $("html, body").scrollTop(position);
                }
            }
        };

    menu_init();
    social_sharing_init();
    landing_page_init();
    persona_menu_init();
    select_nav_init();
    $(".blog-preview").equal_height();
    $(".video-section .widget-span").equal_height();
    $(".landing-page .main-content, .landing-page .sidebar").equal_height();
    $(".amenities-and-services__items").equal_height();
    $(".blog-post--roll").equal_height();
    $(".blog-feature__locations, .blog-feature__roll").equal_height();
    $(".video-section__thumb a, .google-map__directions").magnificPopup({
        type: 'iframe'
    });
    $(".photo-gallery").magnificPopup({
        delegate: '.photo-gallery__photo',
        type: 'image',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1]
        }
    });
    $(".photo-gallery__placeholder").click(function () {
        $(this).closest(".photo-gallery").magnificPopup("open");
    });


    if (!is_editor()) {
        home_banner_init();
        sticky_footer_init();
        $(".tabbed-section").tabify({
            equalize_height: true,
            on_change: function () {
                $(window).trigger("resize");
            }
        });
    }

    body.waypoint(function (direction) {
        header.toggleClass("site-header--scrolled", direction === "down");
    }, { offset: -utility_height });

    $(".waypoint").waypoint(function () {
        $(this).addClass("waypoint--active");
        $(this).waypoint("destroy");
    }, { offset: function () {
        return $(window).height() / 1.25;
    }});

    $("table").wrap("<div class='table-wrapper'></div>");

    $(".slider--wifi .slider__slide").equal_height();
    $(".slider").each(function () {
        var slider = $(this);
        slider.flexslider({
            namespace: "slider__",
            selector: ".slider__slides > .slider__slide",
            slideshow: slider.data("autoplay") === true,
            controlNav: slider.data("controlnav") === false ? false : true,
            animation: "slide"
        });
    });

    $(".parallax-window").parallax({
        iosFix: false,
        androidFix: false
    });
    $(".site-footer__menu").parallax({
        imageSrc: "http://cdn2.hubspot.net/hubfs/338227/styles/background-iphone-and-waterbottle.jpg",
        iosFix: false,
        androidFix: false
    });
    $(".site-footer__cta").parallax({
        imageSrc: "http://cdn2.hubspot.net/hubfs/338227/styles/background-abs-female.jpg",
        iosFix: false,
        androidFix: false
    });
    $(".blog-feature__locations").parallax({
        imageSrc: "http://cdn2.hubspot.net/hubfs/338227/site/background-treadmill.jpg",
        iosFix: false,
        androidFix: false
    });

    if ($("#twitter-quote").length > 0) {
        twitterFetcher.fetch({
            id:                 "615930141873709056",
            domId:              "twitter-quote",
            maxTweets:          4,
            showUser:           false,
            showRetweet:        false,
            showInteraction:    false,
            showImages:         false
        });
    }
    if ($("#instafeed").length > 0) {
        instagram_feed  = new Instafeed({
            get: 'user',
            userId: 222351265,
            clientId: 'afdeb3782faf4efc8fb99f390cd0f276',
            accessToken:'222351265.afdeb37.08996b834cac472b977a983696976b6c',
            limit: 8,
            resolution: 'low_resolution'
        });
        instagram_feed.run();
    }

    parallax_refresh();

    // http://css-tricks.com/snippets/jquery/smooth-scrolling/
    $("body").data("scrolling", false);
    $('a[href*=#]:not([href=#])').click(function (e) {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname && $(this).data("no_scroll") !== true) {
            var target = $(this.hash),
                offset = 0,
                header_test = header.css("position") === "absolute" || header.css("position") === "fixed";
            e.preventDefault();
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                offset = target.offset().top;
                offset = header_test ? offset - scrolled_height : offset;
                $("body").data("scrolling", true);
                $('html, body').animate({
                    scrollTop: offset
                }, 1000,
                function () {
                    $("body").data("scrolling", false);
                });
            }
        }
    });

    $(document).ajaxComplete(parallax_refresh);
    $(window)
        .on("load", function () {
            scroll_to_hash();
            subscribe_form_init();
            back_to_top_init();
            window.setInterval(doc_height_refresh, 100);
        })
        .resize(function () {
            var new_width = $(window).width(),
                new_height = $(window).height();
            if (window_width !== new_width || window_height !== new_height) {
                window_width = new_width;
                window_height = new_height;
                $(window).trigger("width_change");
            }
        })
        .on("orientationchange", function () {
            $(window).trigger("width_change");
        })
        .on("motionvibe_loaded", function () {
            $(window).trigger("resize");
        });
});

/*!

*/

$.fn.equal_height = function (natural_mobile, on_set) {
    "use strict";
    if (typeof on_set === "undefined") {
        on_set = function (x) { return x; };
    }
    if (typeof natural_mobile === "undefined") {
        natural_mobile = true;
    }

    var els = this,
        equal_class = "equalized",
        set_height = function () {
            var max = 0;
            els.each(function () {
                var el = $(this),
                    display = el.css("display"),
                    height;
                el
                    .removeClass(equal_class)
                    .css("height", "");
                if (display === "none") {
                    el.css("display", "block");
                }
                if (el.css("box-sizing") === "border-box") {
                    height = el.outerHeight();
                } else {
                    height = el.height();
                }
                el.css("display", "");
                if (height > max) {
                    max = height;
                }
            });
            on_set(max);
            els
                .addClass(equal_class)
                .css("height", max);
        };

    if (els.length > 1) {
        if (natural_mobile) {
            els.addClass("natural-mobile");
        }
        set_height();
        $(window).on("width_change load", set_height);
    }

    return this;
};
// Generated by CoffeeScript 1.3.3
/* jshint ignore:start */

(function() {
  var Instafeed, root;

  Instafeed = (function() {

    function Instafeed(params, context) {
      var option, value;
      this.options = {
        target: 'instafeed',
        get: 'popular',
        resolution: 'thumbnail',
        sortBy: 'none',
        links: true,
        mock: false,
        useHttp: false
      };
      if (typeof params === 'object') {
        for (option in params) {
          value = params[option];
          this.options[option] = value;
        }
      }
      this.context = context != null ? context : this;
      this.unique = this._genKey();
    }

    Instafeed.prototype.hasNext = function() {
      return typeof this.context.nextUrl === 'string' && this.context.nextUrl.length > 0;
    };

    Instafeed.prototype.next = function() {
      if (!this.hasNext()) {
        return false;
      }
      return this.run(this.context.nextUrl);
    };

    Instafeed.prototype.run = function(url) {
      var header, instanceName, script;
      if (typeof this.options.clientId !== 'string') {
        if (typeof this.options.accessToken !== 'string') {
          throw new Error("Missing clientId or accessToken.");
        }
      }
      if (typeof this.options.accessToken !== 'string') {
        if (typeof this.options.clientId !== 'string') {
          throw new Error("Missing clientId or accessToken.");
        }
      }
      if ((this.options.before != null) && typeof this.options.before === 'function') {
        this.options.before.call(this);
      }
      if (typeof document !== "undefined" && document !== null) {
        script = document.createElement('script');
        script.id = 'instafeed-fetcher';
        script.src = url || this._buildUrl();
        header = document.getElementsByTagName('head');
        header[0].appendChild(script);
        instanceName = "instafeedCache" + this.unique;
        window[instanceName] = new Instafeed(this.options, this);
        window[instanceName].unique = this.unique;
      }
      return true;
    };

    Instafeed.prototype.parse = function(response) {
      var anchor, fragment, header, htmlString, image, imageString, imageUrl, images, img, imgUrl, instanceName, node, reverse, sortSettings, tmpEl, _i, _j, _k, _len, _len1, _len2, _ref;
      if (typeof response !== 'object') {
        if ((this.options.error != null) && typeof this.options.error === 'function') {
          this.options.error.call(this, 'Invalid JSON data');
          return false;
        } else {
          throw new Error('Invalid JSON response');
        }
      }
      if (response.meta.code !== 200) {
        if ((this.options.error != null) && typeof this.options.error === 'function') {
          this.options.error.call(this, response.meta.error_message);
          return false;
        } else {
          throw new Error("Error from Instagram: " + response.meta.error_message);
        }
      }
      if (response.data.length === 0) {
        if ((this.options.error != null) && typeof this.options.error === 'function') {
          this.options.error.call(this, 'No images were returned from Instagram');
          return false;
        } else {
          throw new Error('No images were returned from Instagram');
        }
      }
      if ((this.options.success != null) && typeof this.options.success === 'function') {
        this.options.success.call(this, response);
      }
      this.context.nextUrl = '';
      if (response.pagination != null) {
        this.context.nextUrl = response.pagination.next_url;
      }
      if (this.options.sortBy !== 'none') {
        if (this.options.sortBy === 'random') {
          sortSettings = ['', 'random'];
        } else {
          sortSettings = this.options.sortBy.split('-');
        }
        reverse = sortSettings[0] === 'least' ? true : false;
        switch (sortSettings[1]) {
          case 'random':
            response.data.sort(function() {
              return 0.5 - Math.random();
            });
            break;
          case 'recent':
            response.data = this._sortBy(response.data, 'created_time', reverse);
            break;
          case 'liked':
            response.data = this._sortBy(response.data, 'likes.count', reverse);
            break;
          case 'commented':
            response.data = this._sortBy(response.data, 'comments.count', reverse);
            break;
          default:
            throw new Error("Invalid option for sortBy: '" + this.options.sortBy + "'.");
        }
      }
      if ((typeof document !== "undefined" && document !== null) && this.options.mock === false) {
        images = response.data;
        if (this.options.limit != null) {
          if (images.length > this.options.limit) {
            images = images.slice(0, this.options.limit + 1 || 9e9);
          }
        }
        fragment = document.createDocumentFragment();
        if ((this.options.filter != null) && typeof this.options.filter === 'function') {
          images = this._filter(images, this.options.filter);
        }
        if ((this.options.template != null) && typeof this.options.template === 'string') {
          htmlString = '';
          imageString = '';
          imgUrl = '';
          tmpEl = document.createElement('div');
          for (_i = 0, _len = images.length; _i < _len; _i++) {
            image = images[_i];
            imageUrl = image.images[this.options.resolution].url;
            if (!this.options.useHttp) {
              imageUrl = imageUrl.replace('http://', '//');
            }
            imageString = this._makeTemplate(this.options.template, {
              model: image,
              id: image.id,
              link: image.link,
              image: imageUrl,
              caption: this._getObjectProperty(image, 'caption.text'),
              likes: image.likes.count,
              comments: image.comments.count,
              location: this._getObjectProperty(image, 'location.name')
            });
            htmlString += imageString;
          }
          tmpEl.innerHTML = htmlString;
          _ref = [].slice.call(tmpEl.childNodes);
          for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
            node = _ref[_j];
            fragment.appendChild(node);
          }
        } else {
          for (_k = 0, _len2 = images.length; _k < _len2; _k++) {
            image = images[_k];
            img = document.createElement('img');
            imageUrl = image.images[this.options.resolution].url;
            if (!this.options.useHttp) {
              imageUrl = imageUrl.replace('http://', '//');
            }
            img.src = imageUrl;
            if (this.options.links === true) {
              anchor = document.createElement('a');
              anchor.href = image.link;
              anchor.appendChild(img);
              fragment.appendChild(anchor);
            } else {
              fragment.appendChild(img);
            }
          }
        }
        document.getElementById(this.options.target).appendChild(fragment);
        header = document.getElementsByTagName('head')[0];
        header.removeChild(document.getElementById('instafeed-fetcher'));
        instanceName = "instafeedCache" + this.unique;
        window[instanceName] = void 0;
        try {
          delete window[instanceName];
        } catch (e) {

        }
      }
      if ((this.options.after != null) && typeof this.options.after === 'function') {
        this.options.after.call(this);
      }
      return true;
    };

    Instafeed.prototype._buildUrl = function() {
      var base, endpoint, final;
      base = "https://api.instagram.com/v1";
      switch (this.options.get) {
        case "popular":
          endpoint = "media/popular";
          break;
        case "tagged":
          if (typeof this.options.tagName !== 'string') {
            throw new Error("No tag name specified. Use the 'tagName' option.");
          }
          endpoint = "tags/" + this.options.tagName + "/media/recent";
          break;
        case "location":
          if (typeof this.options.locationId !== 'number') {
            throw new Error("No location specified. Use the 'locationId' option.");
          }
          endpoint = "locations/" + this.options.locationId + "/media/recent";
          break;
        case "user":
          if (typeof this.options.userId !== 'number') {
            throw new Error("No user specified. Use the 'userId' option.");
          }
          if (typeof this.options.accessToken !== 'string') {
            throw new Error("No access token. Use the 'accessToken' option.");
          }
          endpoint = "users/" + this.options.userId + "/media/recent";
          break;
        default:
          throw new Error("Invalid option for get: '" + this.options.get + "'.");
      }
      final = "" + base + "/" + endpoint;
      if (this.options.accessToken != null) {
        final += "?access_token=" + this.options.accessToken;
      } else {
        final += "?client_id=" + this.options.clientId;
      }
      if (this.options.limit != null) {
        final += "&count=" + this.options.limit;
      }
      final += "&callback=instafeedCache" + this.unique + ".parse";
      return final;
    };

    Instafeed.prototype._genKey = function() {
      var S4;
      S4 = function() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      };
      return "" + (S4()) + (S4()) + (S4()) + (S4());
    };

    Instafeed.prototype._makeTemplate = function(template, data) {
      var output, pattern, varName, varValue, _ref;
      pattern = /(?:\{{2})([\w\[\]\.]+)(?:\}{2})/;
      output = template;
      while (pattern.test(output)) {
        varName = output.match(pattern)[1];
        varValue = (_ref = this._getObjectProperty(data, varName)) != null ? _ref : '';
        output = output.replace(pattern, "" + varValue);
      }
      return output;
    };

    Instafeed.prototype._getObjectProperty = function(object, property) {
      var piece, pieces;
      property = property.replace(/\[(\w+)\]/g, '.$1');
      pieces = property.split('.');
      while (pieces.length) {
        piece = pieces.shift();
        if ((object != null) && piece in object) {
          object = object[piece];
        } else {
          return null;
        }
      }
      return object;
    };

    Instafeed.prototype._sortBy = function(data, property, reverse) {
      var sorter;
      sorter = function(a, b) {
        var valueA, valueB;
        valueA = this._getObjectProperty(a, property);
        valueB = this._getObjectProperty(b, property);
        if (reverse) {
          if (valueA > valueB) {
            return 1;
          } else {
            return -1;
          }
        }
        if (valueA < valueB) {
          return 1;
        } else {
          return -1;
        }
      };
      data.sort(sorter.bind(this));
      return data;
    };

    Instafeed.prototype._filter = function(images, filter) {
      var filteredImages, image, _fn, _i, _len;
      filteredImages = [];
      _fn = function(image) {
        if (filter(image)) {
          return filteredImages.push(image);
        }
      };
      for (_i = 0, _len = images.length; _i < _len; _i++) {
        image = images[_i];
        _fn(image);
      }
      return filteredImages;
    };

    return Instafeed;

  })();

  root = typeof exports !== "undefined" && exports !== null ? exports : window;

  root.Instafeed = Instafeed;

}).call(this);

/*! Magnific Popup - v1.0.0 - 2015-01-03
* http://dimsemenov.com/plugins/magnific-popup/
* Copyright (c) 2015 Dmitry Semenov; */
/* jshint ignore:start */
;(function (factory) { 
if (typeof define === 'function' && define.amd) { 
 // AMD. Register as an anonymous module. 
 define(['jquery'], factory); 
 } else if (typeof exports === 'object') { 
 // Node/CommonJS 
 factory(require('jquery')); 
 } else { 
 // Browser globals 
 factory(window.jQuery || window.Zepto); 
 } 
 }(function($) { 

/*>>core*/
/**
 * 
 * Magnific Popup Core JS file
 * 
 */


/**
 * Private static constants
 */
var CLOSE_EVENT = 'Close',
    BEFORE_CLOSE_EVENT = 'BeforeClose',
    AFTER_CLOSE_EVENT = 'AfterClose',
    BEFORE_APPEND_EVENT = 'BeforeAppend',
    MARKUP_PARSE_EVENT = 'MarkupParse',
    OPEN_EVENT = 'Open',
    CHANGE_EVENT = 'Change',
    NS = 'mfp',
    EVENT_NS = '.' + NS,
    READY_CLASS = 'mfp-ready',
    REMOVING_CLASS = 'mfp-removing',
    PREVENT_CLOSE_CLASS = 'mfp-prevent-close';


/**
 * Private vars 
 */
/*jshint -W079 */
var mfp, // As we have only one instance of MagnificPopup object, we define it locally to not to use 'this'
    MagnificPopup = function(){},
    _isJQ = !!(window.jQuery),
    _prevStatus,
    _window = $(window),
    _document,
    _prevContentType,
    _wrapClasses,
    _currPopupType;


/**
 * Private functions
 */
var _mfpOn = function(name, f) {
        mfp.ev.on(NS + name + EVENT_NS, f);
    },
    _getEl = function(className, appendTo, html, raw) {
        var el = document.createElement('div');
        el.className = 'mfp-'+className;
        if(html) {
            el.innerHTML = html;
        }
        if(!raw) {
            el = $(el);
            if(appendTo) {
                el.appendTo(appendTo);
            }
        } else if(appendTo) {
            appendTo.appendChild(el);
        }
        return el;
    },
    _mfpTrigger = function(e, data) {
        mfp.ev.triggerHandler(NS + e, data);

        if(mfp.st.callbacks) {
            // converts "mfpEventName" to "eventName" callback and triggers it if it's present
            e = e.charAt(0).toLowerCase() + e.slice(1);
            if(mfp.st.callbacks[e]) {
                mfp.st.callbacks[e].apply(mfp, $.isArray(data) ? data : [data]);
            }
        }
    },
    _getCloseBtn = function(type) {
        if(type !== _currPopupType || !mfp.currTemplate.closeBtn) {
            mfp.currTemplate.closeBtn = $( mfp.st.closeMarkup.replace('%title%', mfp.st.tClose ) );
            _currPopupType = type;
        }
        return mfp.currTemplate.closeBtn;
    },
    // Initialize Magnific Popup only when called at least once
    _checkInstance = function() {
        if(!$.magnificPopup.instance) {
            /*jshint -W020 */
            mfp = new MagnificPopup();
            mfp.init();
            $.magnificPopup.instance = mfp;
        }
    },
    // CSS transition detection, http://stackoverflow.com/questions/7264899/detect-css-transitions-using-javascript-and-without-modernizr
    supportsTransitions = function() {
        var s = document.createElement('p').style, // 's' for style. better to create an element if body yet to exist
            v = ['ms','O','Moz','Webkit']; // 'v' for vendor

        if( s['transition'] !== undefined ) {
            return true; 
        }
            
        while( v.length ) {
            if( v.pop() + 'Transition' in s ) {
                return true;
            }
        }
                
        return false;
    };



/**
 * Public functions
 */
MagnificPopup.prototype = {

    constructor: MagnificPopup,

    /**
     * Initializes Magnific Popup plugin. 
     * This function is triggered only once when $.fn.magnificPopup or $.magnificPopup is executed
     */
    init: function() {
        var appVersion = navigator.appVersion;
        mfp.isIE7 = appVersion.indexOf("MSIE 7.") !== -1; 
        mfp.isIE8 = appVersion.indexOf("MSIE 8.") !== -1;
        mfp.isLowIE = mfp.isIE7 || mfp.isIE8;
        mfp.isAndroid = (/android/gi).test(appVersion);
        mfp.isIOS = (/iphone|ipad|ipod/gi).test(appVersion);
        mfp.supportsTransition = supportsTransitions();

        // We disable fixed positioned lightbox on devices that don't handle it nicely.
        // If you know a better way of detecting this - let me know.
        mfp.probablyMobile = (mfp.isAndroid || mfp.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent) );
        _document = $(document);

        mfp.popupsCache = {};
    },

    /**
     * Opens popup
     * @param  data [description]
     */
    open: function(data) {

        var i;

        if(data.isObj === false) { 
            // convert jQuery collection to array to avoid conflicts later
            mfp.items = data.items.toArray();

            mfp.index = 0;
            var items = data.items,
                item;
            for(i = 0; i < items.length; i++) {
                item = items[i];
                if(item.parsed) {
                    item = item.el[0];
                }
                if(item === data.el[0]) {
                    mfp.index = i;
                    break;
                }
            }
        } else {
            mfp.items = $.isArray(data.items) ? data.items : [data.items];
            mfp.index = data.index || 0;
        }

        // if popup is already opened - we just update the content
        if(mfp.isOpen) {
            mfp.updateItemHTML();
            return;
        }
        
        mfp.types = []; 
        _wrapClasses = '';
        if(data.mainEl && data.mainEl.length) {
            mfp.ev = data.mainEl.eq(0);
        } else {
            mfp.ev = _document;
        }

        if(data.key) {
            if(!mfp.popupsCache[data.key]) {
                mfp.popupsCache[data.key] = {};
            }
            mfp.currTemplate = mfp.popupsCache[data.key];
        } else {
            mfp.currTemplate = {};
        }



        mfp.st = $.extend(true, {}, $.magnificPopup.defaults, data ); 
        mfp.fixedContentPos = mfp.st.fixedContentPos === 'auto' ? !mfp.probablyMobile : mfp.st.fixedContentPos;

        if(mfp.st.modal) {
            mfp.st.closeOnContentClick = false;
            mfp.st.closeOnBgClick = false;
            mfp.st.showCloseBtn = false;
            mfp.st.enableEscapeKey = false;
        }
        

        // Building markup
        // main containers are created only once
        if(!mfp.bgOverlay) {

            // Dark overlay
            mfp.bgOverlay = _getEl('bg').on('click'+EVENT_NS, function() {
                mfp.close();
            });

            mfp.wrap = _getEl('wrap').attr('tabindex', -1).on('click'+EVENT_NS, function(e) {
                if(mfp._checkIfClose(e.target)) {
                    mfp.close();
                }
            });

            mfp.container = _getEl('container', mfp.wrap);
        }

        mfp.contentContainer = _getEl('content');
        if(mfp.st.preloader) {
            mfp.preloader = _getEl('preloader', mfp.container, mfp.st.tLoading);
        }


        // Initializing modules
        var modules = $.magnificPopup.modules;
        for(i = 0; i < modules.length; i++) {
            var n = modules[i];
            n = n.charAt(0).toUpperCase() + n.slice(1);
            mfp['init'+n].call(mfp);
        }
        _mfpTrigger('BeforeOpen');


        if(mfp.st.showCloseBtn) {
            // Close button
            if(!mfp.st.closeBtnInside) {
                mfp.wrap.append( _getCloseBtn() );
            } else {
                _mfpOn(MARKUP_PARSE_EVENT, function(e, template, values, item) {
                    values.close_replaceWith = _getCloseBtn(item.type);
                });
                _wrapClasses += ' mfp-close-btn-in';
            }
        }

        if(mfp.st.alignTop) {
            _wrapClasses += ' mfp-align-top';
        }

    

        if(mfp.fixedContentPos) {
            mfp.wrap.css({
                overflow: mfp.st.overflowY,
                overflowX: 'hidden',
                overflowY: mfp.st.overflowY
            });
        } else {
            mfp.wrap.css({ 
                top: _window.scrollTop(),
                position: 'absolute'
            });
        }
        if( mfp.st.fixedBgPos === false || (mfp.st.fixedBgPos === 'auto' && !mfp.fixedContentPos) ) {
            mfp.bgOverlay.css({
                height: _document.height(),
                position: 'absolute'
            });
        }

        

        if(mfp.st.enableEscapeKey) {
            // Close on ESC key
            _document.on('keyup' + EVENT_NS, function(e) {
                if(e.keyCode === 27) {
                    mfp.close();
                }
            });
        }

        _window.on('resize' + EVENT_NS, function() {
            mfp.updateSize();
        });


        if(!mfp.st.closeOnContentClick) {
            _wrapClasses += ' mfp-auto-cursor';
        }
        
        if(_wrapClasses)
            mfp.wrap.addClass(_wrapClasses);


        // this triggers recalculation of layout, so we get it once to not to trigger twice
        var windowHeight = mfp.wH = _window.height();

        
        var windowStyles = {};

        if( mfp.fixedContentPos ) {
            if(mfp._hasScrollBar(windowHeight)){
                var s = mfp._getScrollbarSize();
                if(s) {
                    windowStyles.marginRight = s;
                }
            }
        }

        if(mfp.fixedContentPos) {
            if(!mfp.isIE7) {
                windowStyles.overflow = 'hidden';
            } else {
                // ie7 double-scroll bug
                $('body, html').css('overflow', 'hidden');
            }
        }

        
        
        var classesToadd = mfp.st.mainClass;
        if(mfp.isIE7) {
            classesToadd += ' mfp-ie7';
        }
        if(classesToadd) {
            mfp._addClassToMFP( classesToadd );
        }

        // add content
        mfp.updateItemHTML();

        _mfpTrigger('BuildControls');

        // remove scrollbar, add margin e.t.c
        $('html').css(windowStyles);
        
        // add everything to DOM
        mfp.bgOverlay.add(mfp.wrap).prependTo( mfp.st.prependTo || $(document.body) );

        // Save last focused element
        mfp._lastFocusedEl = document.activeElement;
        
        // Wait for next cycle to allow CSS transition
        setTimeout(function() {
            
            if(mfp.content) {
                mfp._addClassToMFP(READY_CLASS);
                mfp._setFocus();
            } else {
                // if content is not defined (not loaded e.t.c) we add class only for BG
                mfp.bgOverlay.addClass(READY_CLASS);
            }
            
            // Trap the focus in popup
            _document.on('focusin' + EVENT_NS, mfp._onFocusIn);

        }, 16);

        mfp.isOpen = true;
        mfp.updateSize(windowHeight);
        _mfpTrigger(OPEN_EVENT);

        return data;
    },

    /**
     * Closes the popup
     */
    close: function() {
        if(!mfp.isOpen) return;
        _mfpTrigger(BEFORE_CLOSE_EVENT);

        mfp.isOpen = false;
        // for CSS3 animation
        if(mfp.st.removalDelay && !mfp.isLowIE && mfp.supportsTransition )  {
            mfp._addClassToMFP(REMOVING_CLASS);
            setTimeout(function() {
                mfp._close();
            }, mfp.st.removalDelay);
        } else {
            mfp._close();
        }
    },

    /**
     * Helper for close() function
     */
    _close: function() {
        _mfpTrigger(CLOSE_EVENT);

        var classesToRemove = REMOVING_CLASS + ' ' + READY_CLASS + ' ';

        mfp.bgOverlay.detach();
        mfp.wrap.detach();
        mfp.container.empty();

        if(mfp.st.mainClass) {
            classesToRemove += mfp.st.mainClass + ' ';
        }

        mfp._removeClassFromMFP(classesToRemove);

        if(mfp.fixedContentPos) {
            var windowStyles = {marginRight: ''};
            if(mfp.isIE7) {
                $('body, html').css('overflow', '');
            } else {
                windowStyles.overflow = '';
            }
            $('html').css(windowStyles);
        }
        
        _document.off('keyup' + EVENT_NS + ' focusin' + EVENT_NS);
        mfp.ev.off(EVENT_NS);

        // clean up DOM elements that aren't removed
        mfp.wrap.attr('class', 'mfp-wrap').removeAttr('style');
        mfp.bgOverlay.attr('class', 'mfp-bg');
        mfp.container.attr('class', 'mfp-container');

        // remove close button from target element
        if(mfp.st.showCloseBtn &&
        (!mfp.st.closeBtnInside || mfp.currTemplate[mfp.currItem.type] === true)) {
            if(mfp.currTemplate.closeBtn)
                mfp.currTemplate.closeBtn.detach();
        }


        if(mfp._lastFocusedEl) {
            $(mfp._lastFocusedEl).focus(); // put tab focus back
        }
        mfp.currItem = null;    
        mfp.content = null;
        mfp.currTemplate = null;
        mfp.prevHeight = 0;

        _mfpTrigger(AFTER_CLOSE_EVENT);
    },
    
    updateSize: function(winHeight) {

        if(mfp.isIOS) {
            // fixes iOS nav bars https://github.com/dimsemenov/Magnific-Popup/issues/2
            var zoomLevel = document.documentElement.clientWidth / window.innerWidth;
            var height = window.innerHeight * zoomLevel;
            mfp.wrap.css('height', height);
            mfp.wH = height;
        } else {
            mfp.wH = winHeight || _window.height();
        }
        // Fixes #84: popup incorrectly positioned with position:relative on body
        if(!mfp.fixedContentPos) {
            mfp.wrap.css('height', mfp.wH);
        }

        _mfpTrigger('Resize');

    },

    /**
     * Set content of popup based on current index
     */
    updateItemHTML: function() {
        var item = mfp.items[mfp.index];

        // Detach and perform modifications
        mfp.contentContainer.detach();

        if(mfp.content)
            mfp.content.detach();

        if(!item.parsed) {
            item = mfp.parseEl( mfp.index );
        }

        var type = item.type;   

        _mfpTrigger('BeforeChange', [mfp.currItem ? mfp.currItem.type : '', type]);
        // BeforeChange event works like so:
        // _mfpOn('BeforeChange', function(e, prevType, newType) { });
        
        mfp.currItem = item;

        

        

        if(!mfp.currTemplate[type]) {
            var markup = mfp.st[type] ? mfp.st[type].markup : false;

            // allows to modify markup
            _mfpTrigger('FirstMarkupParse', markup);

            if(markup) {
                mfp.currTemplate[type] = $(markup);
            } else {
                // if there is no markup found we just define that template is parsed
                mfp.currTemplate[type] = true;
            }
        }

        if(_prevContentType && _prevContentType !== item.type) {
            mfp.container.removeClass('mfp-'+_prevContentType+'-holder');
        }
        
        var newContent = mfp['get' + type.charAt(0).toUpperCase() + type.slice(1)](item, mfp.currTemplate[type]);
        mfp.appendContent(newContent, type);

        item.preloaded = true;

        _mfpTrigger(CHANGE_EVENT, item);
        _prevContentType = item.type;
        
        // Append container back after its content changed
        mfp.container.prepend(mfp.contentContainer);

        _mfpTrigger('AfterChange');
    },


    /**
     * Set HTML content of popup
     */
    appendContent: function(newContent, type) {
        mfp.content = newContent;
        
        if(newContent) {
            if(mfp.st.showCloseBtn && mfp.st.closeBtnInside &&
                mfp.currTemplate[type] === true) {
                // if there is no markup, we just append close button element inside
                if(!mfp.content.find('.mfp-close').length) {
                    mfp.content.append(_getCloseBtn());
                }
            } else {
                mfp.content = newContent;
            }
        } else {
            mfp.content = '';
        }

        _mfpTrigger(BEFORE_APPEND_EVENT);
        mfp.container.addClass('mfp-'+type+'-holder');

        mfp.contentContainer.append(mfp.content);
    },



    
    /**
     * Creates Magnific Popup data object based on given data
     * @param  {int} index Index of item to parse
     */
    parseEl: function(index) {
        var item = mfp.items[index],
            type;

        if(item.tagName) {
            item = { el: $(item) };
        } else {
            type = item.type;
            item = { data: item, src: item.src };
        }

        if(item.el) {
            var types = mfp.types;

            // check for 'mfp-TYPE' class
            for(var i = 0; i < types.length; i++) {
                if( item.el.hasClass('mfp-'+types[i]) ) {
                    type = types[i];
                    break;
                }
            }

            item.src = item.el.attr('data-mfp-src');
            if(!item.src) {
                item.src = item.el.attr('href');
            }
        }

        item.type = type || mfp.st.type || 'inline';
        item.index = index;
        item.parsed = true;
        mfp.items[index] = item;
        _mfpTrigger('ElementParse', item);

        return mfp.items[index];
    },


    /**
     * Initializes single popup or a group of popups
     */
    addGroup: function(el, options) {
        var eHandler = function(e) {
            e.mfpEl = this;
            mfp._openClick(e, el, options);
        };

        if(!options) {
            options = {};
        } 

        var eName = 'click.magnificPopup';
        options.mainEl = el;
        
        if(options.items) {
            options.isObj = true;
            el.off(eName).on(eName, eHandler);
        } else {
            options.isObj = false;
            if(options.delegate) {
                el.off(eName).on(eName, options.delegate , eHandler);
            } else {
                options.items = el;
                el.off(eName).on(eName, eHandler);
            }
        }
    },
    _openClick: function(e, el, options) {
        var midClick = options.midClick !== undefined ? options.midClick : $.magnificPopup.defaults.midClick;


        if(!midClick && ( e.which === 2 || e.ctrlKey || e.metaKey ) ) {
            return;
        }

        var disableOn = options.disableOn !== undefined ? options.disableOn : $.magnificPopup.defaults.disableOn;

        if(disableOn) {
            if($.isFunction(disableOn)) {
                if( !disableOn.call(mfp) ) {
                    return true;
                }
            } else { // else it's number
                if( _window.width() < disableOn ) {
                    return true;
                }
            }
        }
        
        if(e.type) {
            e.preventDefault();

            // This will prevent popup from closing if element is inside and popup is already opened
            if(mfp.isOpen) {
                e.stopPropagation();
            }
        }
            

        options.el = $(e.mfpEl);
        if(options.delegate) {
            options.items = el.find(options.delegate);
        }
        mfp.open(options);
    },


    /**
     * Updates text on preloader
     */
    updateStatus: function(status, text) {

        if(mfp.preloader) {
            if(_prevStatus !== status) {
                mfp.container.removeClass('mfp-s-'+_prevStatus);
            }

            if(!text && status === 'loading') {
                text = mfp.st.tLoading;
            }

            var data = {
                status: status,
                text: text
            };
            // allows to modify status
            _mfpTrigger('UpdateStatus', data);

            status = data.status;
            text = data.text;

            mfp.preloader.html(text);

            mfp.preloader.find('a').on('click', function(e) {
                e.stopImmediatePropagation();
            });

            mfp.container.addClass('mfp-s-'+status);
            _prevStatus = status;
        }
    },


    /*
        "Private" helpers that aren't private at all
     */
    // Check to close popup or not
    // "target" is an element that was clicked
    _checkIfClose: function(target) {

        if($(target).hasClass(PREVENT_CLOSE_CLASS)) {
            return;
        }

        var closeOnContent = mfp.st.closeOnContentClick;
        var closeOnBg = mfp.st.closeOnBgClick;

        if(closeOnContent && closeOnBg) {
            return true;
        } else {

            // We close the popup if click is on close button or on preloader. Or if there is no content.
            if(!mfp.content || $(target).hasClass('mfp-close') || (mfp.preloader && target === mfp.preloader[0]) ) {
                return true;
            }

            // if click is outside the content
            if(  (target !== mfp.content[0] && !$.contains(mfp.content[0], target))  ) {
                if(closeOnBg) {
                    // last check, if the clicked element is in DOM, (in case it's removed onclick)
                    if( $.contains(document, target) ) {
                        return true;
                    }
                }
            } else if(closeOnContent) {
                return true;
            }

        }
        return false;
    },
    _addClassToMFP: function(cName) {
        mfp.bgOverlay.addClass(cName);
        mfp.wrap.addClass(cName);
    },
    _removeClassFromMFP: function(cName) {
        this.bgOverlay.removeClass(cName);
        mfp.wrap.removeClass(cName);
    },
    _hasScrollBar: function(winHeight) {
        return (  (mfp.isIE7 ? _document.height() : document.body.scrollHeight) > (winHeight || _window.height()) );
    },
    _setFocus: function() {
        (mfp.st.focus ? mfp.content.find(mfp.st.focus).eq(0) : mfp.wrap).focus();
    },
    _onFocusIn: function(e) {
        if( e.target !== mfp.wrap[0] && !$.contains(mfp.wrap[0], e.target) ) {
            mfp._setFocus();
            return false;
        }
    },
    _parseMarkup: function(template, values, item) {
        var arr;
        if(item.data) {
            values = $.extend(item.data, values);
        }
        _mfpTrigger(MARKUP_PARSE_EVENT, [template, values, item] );

        $.each(values, function(key, value) {
            if(value === undefined || value === false) {
                return true;
            }
            arr = key.split('_');
            if(arr.length > 1) {
                var el = template.find(EVENT_NS + '-'+arr[0]);

                if(el.length > 0) {
                    var attr = arr[1];
                    if(attr === 'replaceWith') {
                        if(el[0] !== value[0]) {
                            el.replaceWith(value);
                        }
                    } else if(attr === 'img') {
                        if(el.is('img')) {
                            el.attr('src', value);
                        } else {
                            el.replaceWith( '<img src="'+value+'" class="' + el.attr('class') + '" />' );
                        }
                    } else {
                        el.attr(arr[1], value);
                    }
                }

            } else {
                template.find(EVENT_NS + '-'+key).html(value);
            }
        });
    },

    _getScrollbarSize: function() {
        // thx David
        if(mfp.scrollbarSize === undefined) {
            var scrollDiv = document.createElement("div");
            scrollDiv.style.cssText = 'width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;';
            document.body.appendChild(scrollDiv);
            mfp.scrollbarSize = scrollDiv.offsetWidth - scrollDiv.clientWidth;
            document.body.removeChild(scrollDiv);
        }
        return mfp.scrollbarSize;
    }

}; /* MagnificPopup core prototype end */




/**
 * Public static functions
 */
$.magnificPopup = {
    instance: null,
    proto: MagnificPopup.prototype,
    modules: [],

    open: function(options, index) {
        _checkInstance();   

        if(!options) {
            options = {};
        } else {
            options = $.extend(true, {}, options);
        }
            

        options.isObj = true;
        options.index = index || 0;
        return this.instance.open(options);
    },

    close: function() {
        return $.magnificPopup.instance && $.magnificPopup.instance.close();
    },

    registerModule: function(name, module) {
        if(module.options) {
            $.magnificPopup.defaults[name] = module.options;
        }
        $.extend(this.proto, module.proto);         
        this.modules.push(name);
    },

    defaults: {   

        // Info about options is in docs:
        // http://dimsemenov.com/plugins/magnific-popup/documentation.html#options
        
        disableOn: 0,   

        key: null,

        midClick: false,

        mainClass: '',

        preloader: true,

        focus: '', // CSS selector of input to focus after popup is opened
        
        closeOnContentClick: false,

        closeOnBgClick: true,

        closeBtnInside: true, 

        showCloseBtn: true,

        enableEscapeKey: true,

        modal: false,

        alignTop: false,
    
        removalDelay: 0,

        prependTo: null,
        
        fixedContentPos: 'auto', 
    
        fixedBgPos: 'auto',

        overflowY: 'auto',

        closeMarkup: '<button title="%title%" type="button" class="mfp-close">&times;</button>',

        tClose: 'Close (Esc)',

        tLoading: 'Loading...'

    }
};



$.fn.magnificPopup = function(options) {
    _checkInstance();

    var jqEl = $(this);

    // We call some API method of first param is a string
    if (typeof options === "string" ) {

        if(options === 'open') {
            var items,
                itemOpts = _isJQ ? jqEl.data('magnificPopup') : jqEl[0].magnificPopup,
                index = parseInt(arguments[1], 10) || 0;

            if(itemOpts.items) {
                items = itemOpts.items[index];
            } else {
                items = jqEl;
                if(itemOpts.delegate) {
                    items = items.find(itemOpts.delegate);
                }
                items = items.eq( index );
            }
            mfp._openClick({mfpEl:items}, jqEl, itemOpts);
        } else {
            if(mfp.isOpen)
                mfp[options].apply(mfp, Array.prototype.slice.call(arguments, 1));
        }

    } else {
        // clone options obj
        options = $.extend(true, {}, options);
        
        /*
         * As Zepto doesn't support .data() method for objects 
         * and it works only in normal browsers
         * we assign "options" object directly to the DOM element. FTW!
         */
        if(_isJQ) {
            jqEl.data('magnificPopup', options);
        } else {
            jqEl[0].magnificPopup = options;
        }

        mfp.addGroup(jqEl, options);

    }
    return jqEl;
};


//Quick benchmark
/*
var start = performance.now(),
    i,
    rounds = 1000;

for(i = 0; i < rounds; i++) {

}
console.log('Test #1:', performance.now() - start);

start = performance.now();
for(i = 0; i < rounds; i++) {

}
console.log('Test #2:', performance.now() - start);
*/


/*>>core*/

/*>>inline*/

var INLINE_NS = 'inline',
    _hiddenClass,
    _inlinePlaceholder, 
    _lastInlineElement,
    _putInlineElementsBack = function() {
        if(_lastInlineElement) {
            _inlinePlaceholder.after( _lastInlineElement.addClass(_hiddenClass) ).detach();
            _lastInlineElement = null;
        }
    };

$.magnificPopup.registerModule(INLINE_NS, {
    options: {
        hiddenClass: 'hide', // will be appended with `mfp-` prefix
        markup: '',
        tNotFound: 'Content not found'
    },
    proto: {

        initInline: function() {
            mfp.types.push(INLINE_NS);

            _mfpOn(CLOSE_EVENT+'.'+INLINE_NS, function() {
                _putInlineElementsBack();
            });
        },

        getInline: function(item, template) {

            _putInlineElementsBack();

            if(item.src) {
                var inlineSt = mfp.st.inline,
                    el = $(item.src);

                if(el.length) {

                    // If target element has parent - we replace it with placeholder and put it back after popup is closed
                    var parent = el[0].parentNode;
                    if(parent && parent.tagName) {
                        if(!_inlinePlaceholder) {
                            _hiddenClass = inlineSt.hiddenClass;
                            _inlinePlaceholder = _getEl(_hiddenClass);
                            _hiddenClass = 'mfp-'+_hiddenClass;
                        }
                        // replace target inline element with placeholder
                        _lastInlineElement = el.after(_inlinePlaceholder).detach().removeClass(_hiddenClass);
                    }

                    mfp.updateStatus('ready');
                } else {
                    mfp.updateStatus('error', inlineSt.tNotFound);
                    el = $('<div>');
                }

                item.inlineElement = el;
                return el;
            }

            mfp.updateStatus('ready');
            mfp._parseMarkup(template, {}, item);
            return template;
        }
    }
});

/*>>inline*/

/*>>ajax*/
var AJAX_NS = 'ajax',
    _ajaxCur,
    _removeAjaxCursor = function() {
        if(_ajaxCur) {
            $(document.body).removeClass(_ajaxCur);
        }
    },
    _destroyAjaxRequest = function() {
        _removeAjaxCursor();
        if(mfp.req) {
            mfp.req.abort();
        }
    };

$.magnificPopup.registerModule(AJAX_NS, {

    options: {
        settings: null,
        cursor: 'mfp-ajax-cur',
        tError: '<a href="%url%">The content</a> could not be loaded.'
    },

    proto: {
        initAjax: function() {
            mfp.types.push(AJAX_NS);
            _ajaxCur = mfp.st.ajax.cursor;

            _mfpOn(CLOSE_EVENT+'.'+AJAX_NS, _destroyAjaxRequest);
            _mfpOn('BeforeChange.' + AJAX_NS, _destroyAjaxRequest);
        },
        getAjax: function(item) {

            if(_ajaxCur) {
                $(document.body).addClass(_ajaxCur);
            }

            mfp.updateStatus('loading');

            var opts = $.extend({
                url: item.src,
                success: function(data, textStatus, jqXHR) {
                    var temp = {
                        data:data,
                        xhr:jqXHR
                    };

                    _mfpTrigger('ParseAjax', temp);

                    mfp.appendContent( $(temp.data), AJAX_NS );

                    item.finished = true;

                    _removeAjaxCursor();

                    mfp._setFocus();

                    setTimeout(function() {
                        mfp.wrap.addClass(READY_CLASS);
                    }, 16);

                    mfp.updateStatus('ready');

                    _mfpTrigger('AjaxContentAdded');
                },
                error: function() {
                    _removeAjaxCursor();
                    item.finished = item.loadError = true;
                    mfp.updateStatus('error', mfp.st.ajax.tError.replace('%url%', item.src));
                }
            }, mfp.st.ajax.settings);

            mfp.req = $.ajax(opts);

            return '';
        }
    }
});





    

/*>>ajax*/

/*>>image*/
var _imgInterval,
    _getTitle = function(item) {
        if(item.data && item.data.title !== undefined) 
            return item.data.title;

        var src = mfp.st.image.titleSrc;

        if(src) {
            if($.isFunction(src)) {
                return src.call(mfp, item);
            } else if(item.el) {
                return item.el.attr(src) || '';
            }
        }
        return '';
    };

$.magnificPopup.registerModule('image', {

    options: {
        markup: '<div class="mfp-figure">'+
                    '<div class="mfp-close"></div>'+
                    '<figure>'+
                        '<div class="mfp-img"></div>'+
                        '<figcaption>'+
                            '<div class="mfp-bottom-bar">'+
                                '<div class="mfp-title"></div>'+
                                '<div class="mfp-counter"></div>'+
                            '</div>'+
                        '</figcaption>'+
                    '</figure>'+
                '</div>',
        cursor: 'mfp-zoom-out-cur',
        titleSrc: 'title', 
        verticalFit: true,
        tError: '<a href="%url%">The image</a> could not be loaded.'
    },

    proto: {
        initImage: function() {
            var imgSt = mfp.st.image,
                ns = '.image';

            mfp.types.push('image');

            _mfpOn(OPEN_EVENT+ns, function() {
                if(mfp.currItem.type === 'image' && imgSt.cursor) {
                    $(document.body).addClass(imgSt.cursor);
                }
            });

            _mfpOn(CLOSE_EVENT+ns, function() {
                if(imgSt.cursor) {
                    $(document.body).removeClass(imgSt.cursor);
                }
                _window.off('resize' + EVENT_NS);
            });

            _mfpOn('Resize'+ns, mfp.resizeImage);
            if(mfp.isLowIE) {
                _mfpOn('AfterChange', mfp.resizeImage);
            }
        },
        resizeImage: function() {
            var item = mfp.currItem;
            if(!item || !item.img) return;

            if(mfp.st.image.verticalFit) {
                var decr = 0;
                // fix box-sizing in ie7/8
                if(mfp.isLowIE) {
                    decr = parseInt(item.img.css('padding-top'), 10) + parseInt(item.img.css('padding-bottom'),10);
                }
                item.img.css('max-height', mfp.wH-decr);
            }
        },
        _onImageHasSize: function(item) {
            if(item.img) {
                
                item.hasSize = true;

                if(_imgInterval) {
                    clearInterval(_imgInterval);
                }
                
                item.isCheckingImgSize = false;

                _mfpTrigger('ImageHasSize', item);

                if(item.imgHidden) {
                    if(mfp.content)
                        mfp.content.removeClass('mfp-loading');
                    
                    item.imgHidden = false;
                }

            }
        },

        /**
         * Function that loops until the image has size to display elements that rely on it asap
         */
        findImageSize: function(item) {

            var counter = 0,
                img = item.img[0],
                mfpSetInterval = function(delay) {

                    if(_imgInterval) {
                        clearInterval(_imgInterval);
                    }
                    // decelerating interval that checks for size of an image
                    _imgInterval = setInterval(function() {
                        if(img.naturalWidth > 0) {
                            mfp._onImageHasSize(item);
                            return;
                        }

                        if(counter > 200) {
                            clearInterval(_imgInterval);
                        }

                        counter++;
                        if(counter === 3) {
                            mfpSetInterval(10);
                        } else if(counter === 40) {
                            mfpSetInterval(50);
                        } else if(counter === 100) {
                            mfpSetInterval(500);
                        }
                    }, delay);
                };

            mfpSetInterval(1);
        },

        getImage: function(item, template) {

            var guard = 0,

                // image load complete handler
                onLoadComplete = function() {
                    if(item) {
                        if (item.img[0].complete) {
                            item.img.off('.mfploader');
                            
                            if(item === mfp.currItem){
                                mfp._onImageHasSize(item);

                                mfp.updateStatus('ready');
                            }

                            item.hasSize = true;
                            item.loaded = true;

                            _mfpTrigger('ImageLoadComplete');
                            
                        }
                        else {
                            // if image complete check fails 200 times (20 sec), we assume that there was an error.
                            guard++;
                            if(guard < 200) {
                                setTimeout(onLoadComplete,100);
                            } else {
                                onLoadError();
                            }
                        }
                    }
                },

                // image error handler
                onLoadError = function() {
                    if(item) {
                        item.img.off('.mfploader');
                        if(item === mfp.currItem){
                            mfp._onImageHasSize(item);
                            mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src) );
                        }

                        item.hasSize = true;
                        item.loaded = true;
                        item.loadError = true;
                    }
                },
                imgSt = mfp.st.image;


            var el = template.find('.mfp-img');
            if(el.length) {
                var img = document.createElement('img');
                img.className = 'mfp-img';
                if(item.el && item.el.find('img').length) {
                    img.alt = item.el.find('img').attr('alt');
                }
                item.img = $(img).on('load.mfploader', onLoadComplete).on('error.mfploader', onLoadError);
                img.src = item.src;

                // without clone() "error" event is not firing when IMG is replaced by new IMG
                // TODO: find a way to avoid such cloning
                if(el.is('img')) {
                    item.img = item.img.clone();
                }

                img = item.img[0];
                if(img.naturalWidth > 0) {
                    item.hasSize = true;
                } else if(!img.width) {                                     
                    item.hasSize = false;
                }
            }

            mfp._parseMarkup(template, {
                title: _getTitle(item),
                img_replaceWith: item.img
            }, item);

            mfp.resizeImage();

            if(item.hasSize) {
                if(_imgInterval) clearInterval(_imgInterval);

                if(item.loadError) {
                    template.addClass('mfp-loading');
                    mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src) );
                } else {
                    template.removeClass('mfp-loading');
                    mfp.updateStatus('ready');
                }
                return template;
            }

            mfp.updateStatus('loading');
            item.loading = true;

            if(!item.hasSize) {
                item.imgHidden = true;
                template.addClass('mfp-loading');
                mfp.findImageSize(item);
            } 

            return template;
        }
    }
});



/*>>image*/

/*>>zoom*/
var hasMozTransform,
    getHasMozTransform = function() {
        if(hasMozTransform === undefined) {
            hasMozTransform = document.createElement('p').style.MozTransform !== undefined;
        }
        return hasMozTransform;     
    };

$.magnificPopup.registerModule('zoom', {

    options: {
        enabled: false,
        easing: 'ease-in-out',
        duration: 300,
        opener: function(element) {
            return element.is('img') ? element : element.find('img');
        }
    },

    proto: {

        initZoom: function() {
            var zoomSt = mfp.st.zoom,
                ns = '.zoom',
                image;
                
            if(!zoomSt.enabled || !mfp.supportsTransition) {
                return;
            }

            var duration = zoomSt.duration,
                getElToAnimate = function(image) {
                    var newImg = image.clone().removeAttr('style').removeAttr('class').addClass('mfp-animated-image'),
                        transition = 'all '+(zoomSt.duration/1000)+'s ' + zoomSt.easing,
                        cssObj = {
                            position: 'fixed',
                            zIndex: 9999,
                            left: 0,
                            top: 0,
                            '-webkit-backface-visibility': 'hidden'
                        },
                        t = 'transition';

                    cssObj['-webkit-'+t] = cssObj['-moz-'+t] = cssObj['-o-'+t] = cssObj[t] = transition;

                    newImg.css(cssObj);
                    return newImg;
                },
                showMainContent = function() {
                    mfp.content.css('visibility', 'visible');
                },
                openTimeout,
                animatedImg;

            _mfpOn('BuildControls'+ns, function() {
                if(mfp._allowZoom()) {

                    clearTimeout(openTimeout);
                    mfp.content.css('visibility', 'hidden');

                    // Basically, all code below does is clones existing image, puts in on top of the current one and animated it
                    
                    image = mfp._getItemToZoom();

                    if(!image) {
                        showMainContent();
                        return;
                    }

                    animatedImg = getElToAnimate(image); 
                    
                    animatedImg.css( mfp._getOffset() );

                    mfp.wrap.append(animatedImg);

                    openTimeout = setTimeout(function() {
                        animatedImg.css( mfp._getOffset( true ) );
                        openTimeout = setTimeout(function() {

                            showMainContent();

                            setTimeout(function() {
                                animatedImg.remove();
                                image = animatedImg = null;
                                _mfpTrigger('ZoomAnimationEnded');
                            }, 16); // avoid blink when switching images 

                        }, duration); // this timeout equals animation duration

                    }, 16); // by adding this timeout we avoid short glitch at the beginning of animation


                    // Lots of timeouts...
                }
            });
            _mfpOn(BEFORE_CLOSE_EVENT+ns, function() {
                if(mfp._allowZoom()) {

                    clearTimeout(openTimeout);

                    mfp.st.removalDelay = duration;

                    if(!image) {
                        image = mfp._getItemToZoom();
                        if(!image) {
                            return;
                        }
                        animatedImg = getElToAnimate(image);
                    }
                    
                    
                    animatedImg.css( mfp._getOffset(true) );
                    mfp.wrap.append(animatedImg);
                    mfp.content.css('visibility', 'hidden');
                    
                    setTimeout(function() {
                        animatedImg.css( mfp._getOffset() );
                    }, 16);
                }

            });

            _mfpOn(CLOSE_EVENT+ns, function() {
                if(mfp._allowZoom()) {
                    showMainContent();
                    if(animatedImg) {
                        animatedImg.remove();
                    }
                    image = null;
                }   
            });
        },

        _allowZoom: function() {
            return mfp.currItem.type === 'image';
        },

        _getItemToZoom: function() {
            if(mfp.currItem.hasSize) {
                return mfp.currItem.img;
            } else {
                return false;
            }
        },

        // Get element postion relative to viewport
        _getOffset: function(isLarge) {
            var el;
            if(isLarge) {
                el = mfp.currItem.img;
            } else {
                el = mfp.st.zoom.opener(mfp.currItem.el || mfp.currItem);
            }

            var offset = el.offset();
            var paddingTop = parseInt(el.css('padding-top'),10);
            var paddingBottom = parseInt(el.css('padding-bottom'),10);
            offset.top -= ( $(window).scrollTop() - paddingTop );


            /*
            
            Animating left + top + width/height looks glitchy in Firefox, but perfect in Chrome. And vice-versa.

             */
            var obj = {
                width: el.width(),
                // fix Zepto height+padding issue
                height: (_isJQ ? el.innerHeight() : el[0].offsetHeight) - paddingBottom - paddingTop
            };

            // I hate to do this, but there is no another option
            if( getHasMozTransform() ) {
                obj['-moz-transform'] = obj['transform'] = 'translate(' + offset.left + 'px,' + offset.top + 'px)';
            } else {
                obj.left = offset.left;
                obj.top = offset.top;
            }
            return obj;
        }

    }
});



/*>>zoom*/

/*>>iframe*/

var IFRAME_NS = 'iframe',
    _emptyPage = '//about:blank',
    
    _fixIframeBugs = function(isShowing) {
        if(mfp.currTemplate[IFRAME_NS]) {
            var el = mfp.currTemplate[IFRAME_NS].find('iframe');
            if(el.length) { 
                // reset src after the popup is closed to avoid "video keeps playing after popup is closed" bug
                if(!isShowing) {
                    el[0].src = _emptyPage;
                }

                // IE8 black screen bug fix
                if(mfp.isIE8) {
                    el.css('display', isShowing ? 'block' : 'none');
                }
            }
        }
    };

$.magnificPopup.registerModule(IFRAME_NS, {

    options: {
        markup: '<div class="mfp-iframe-scaler">'+
                    '<div class="mfp-close"></div>'+
                    '<iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe>'+
                '</div>',

        srcAction: 'iframe_src',

        // we don't care and support only one default type of URL by default
        patterns: {
            youtube: {
                index: 'youtube.com', 
                id: 'v=', 
                src: '//www.youtube.com/embed/%id%?autoplay=1'
            },
            vimeo: {
                index: 'vimeo.com/',
                id: '/',
                src: '//player.vimeo.com/video/%id%?autoplay=1'
            },
            gmaps: {
                index: '//maps.google.',
                src: '%id%&output=embed'
            }
        }
    },

    proto: {
        initIframe: function() {
            mfp.types.push(IFRAME_NS);

            _mfpOn('BeforeChange', function(e, prevType, newType) {
                if(prevType !== newType) {
                    if(prevType === IFRAME_NS) {
                        _fixIframeBugs(); // iframe if removed
                    } else if(newType === IFRAME_NS) {
                        _fixIframeBugs(true); // iframe is showing
                    } 
                }// else {
                    // iframe source is switched, don't do anything
                //}
            });

            _mfpOn(CLOSE_EVENT + '.' + IFRAME_NS, function() {
                _fixIframeBugs();
            });
        },

        getIframe: function(item, template) {
            var embedSrc = item.src;
            var iframeSt = mfp.st.iframe;
                
            $.each(iframeSt.patterns, function() {
                if(embedSrc.indexOf( this.index ) > -1) {
                    if(this.id) {
                        if(typeof this.id === 'string') {
                            embedSrc = embedSrc.substr(embedSrc.lastIndexOf(this.id)+this.id.length, embedSrc.length);
                        } else {
                            embedSrc = this.id.call( this, embedSrc );
                        }
                    }
                    embedSrc = this.src.replace('%id%', embedSrc );
                    return false; // break;
                }
            });
            
            var dataObj = {};
            if(iframeSt.srcAction) {
                dataObj[iframeSt.srcAction] = embedSrc;
            }
            mfp._parseMarkup(template, dataObj, item);

            mfp.updateStatus('ready');

            return template;
        }
    }
});



/*>>iframe*/

/*>>gallery*/
/**
 * Get looped index depending on number of slides
 */
var _getLoopedId = function(index) {
        var numSlides = mfp.items.length;
        if(index > numSlides - 1) {
            return index - numSlides;
        } else  if(index < 0) {
            return numSlides + index;
        }
        return index;
    },
    _replaceCurrTotal = function(text, curr, total) {
        return text.replace(/%curr%/gi, curr + 1).replace(/%total%/gi, total);
    };

$.magnificPopup.registerModule('gallery', {

    options: {
        enabled: false,
        arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
        preload: [0,2],
        navigateByImgClick: true,
        arrows: true,

        tPrev: 'Previous (Left arrow key)',
        tNext: 'Next (Right arrow key)',
        tCounter: '%curr% of %total%'
    },

    proto: {
        initGallery: function() {

            var gSt = mfp.st.gallery,
                ns = '.mfp-gallery',
                supportsFastClick = Boolean($.fn.mfpFastClick);

            mfp.direction = true; // true - next, false - prev
            
            if(!gSt || !gSt.enabled ) return false;

            _wrapClasses += ' mfp-gallery';

            _mfpOn(OPEN_EVENT+ns, function() {

                if(gSt.navigateByImgClick) {
                    mfp.wrap.on('click'+ns, '.mfp-img', function() {
                        if(mfp.items.length > 1) {
                            mfp.next();
                            return false;
                        }
                    });
                }

                _document.on('keydown'+ns, function(e) {
                    if (e.keyCode === 37) {
                        mfp.prev();
                    } else if (e.keyCode === 39) {
                        mfp.next();
                    }
                });
            });

            _mfpOn('UpdateStatus'+ns, function(e, data) {
                if(data.text) {
                    data.text = _replaceCurrTotal(data.text, mfp.currItem.index, mfp.items.length);
                }
            });

            _mfpOn(MARKUP_PARSE_EVENT+ns, function(e, element, values, item) {
                var l = mfp.items.length;
                values.counter = l > 1 ? _replaceCurrTotal(gSt.tCounter, item.index, l) : '';
            });

            _mfpOn('BuildControls' + ns, function() {
                if(mfp.items.length > 1 && gSt.arrows && !mfp.arrowLeft) {
                    var markup = gSt.arrowMarkup,
                        arrowLeft = mfp.arrowLeft = $( markup.replace(/%title%/gi, gSt.tPrev).replace(/%dir%/gi, 'left') ).addClass(PREVENT_CLOSE_CLASS),           
                        arrowRight = mfp.arrowRight = $( markup.replace(/%title%/gi, gSt.tNext).replace(/%dir%/gi, 'right') ).addClass(PREVENT_CLOSE_CLASS);

                    var eName = supportsFastClick ? 'mfpFastClick' : 'click';
                    arrowLeft[eName](function() {
                        mfp.prev();
                    });         
                    arrowRight[eName](function() {
                        mfp.next();
                    }); 

                    // Polyfill for :before and :after (adds elements with classes mfp-a and mfp-b)
                    if(mfp.isIE7) {
                        _getEl('b', arrowLeft[0], false, true);
                        _getEl('a', arrowLeft[0], false, true);
                        _getEl('b', arrowRight[0], false, true);
                        _getEl('a', arrowRight[0], false, true);
                    }

                    mfp.container.append(arrowLeft.add(arrowRight));
                }
            });

            _mfpOn(CHANGE_EVENT+ns, function() {
                if(mfp._preloadTimeout) clearTimeout(mfp._preloadTimeout);

                mfp._preloadTimeout = setTimeout(function() {
                    mfp.preloadNearbyImages();
                    mfp._preloadTimeout = null;
                }, 16);     
            });


            _mfpOn(CLOSE_EVENT+ns, function() {
                _document.off(ns);
                mfp.wrap.off('click'+ns);
            
                if(mfp.arrowLeft && supportsFastClick) {
                    mfp.arrowLeft.add(mfp.arrowRight).destroyMfpFastClick();
                }
                mfp.arrowRight = mfp.arrowLeft = null;
            });

        }, 
        next: function() {
            mfp.direction = true;
            mfp.index = _getLoopedId(mfp.index + 1);
            mfp.updateItemHTML();
        },
        prev: function() {
            mfp.direction = false;
            mfp.index = _getLoopedId(mfp.index - 1);
            mfp.updateItemHTML();
        },
        goTo: function(newIndex) {
            mfp.direction = (newIndex >= mfp.index);
            mfp.index = newIndex;
            mfp.updateItemHTML();
        },
        preloadNearbyImages: function() {
            var p = mfp.st.gallery.preload,
                preloadBefore = Math.min(p[0], mfp.items.length),
                preloadAfter = Math.min(p[1], mfp.items.length),
                i;

            for(i = 1; i <= (mfp.direction ? preloadAfter : preloadBefore); i++) {
                mfp._preloadItem(mfp.index+i);
            }
            for(i = 1; i <= (mfp.direction ? preloadBefore : preloadAfter); i++) {
                mfp._preloadItem(mfp.index-i);
            }
        },
        _preloadItem: function(index) {
            index = _getLoopedId(index);

            if(mfp.items[index].preloaded) {
                return;
            }

            var item = mfp.items[index];
            if(!item.parsed) {
                item = mfp.parseEl( index );
            }

            _mfpTrigger('LazyLoad', item);

            if(item.type === 'image') {
                item.img = $('<img class="mfp-img" />').on('load.mfploader', function() {
                    item.hasSize = true;
                }).on('error.mfploader', function() {
                    item.hasSize = true;
                    item.loadError = true;
                    _mfpTrigger('LazyLoadError', item);
                }).attr('src', item.src);
            }


            item.preloaded = true;
        }
    }
});

/*
Touch Support that might be implemented some day

addSwipeGesture: function() {
    var startX,
        moved,
        multipleTouches;

        return;

    var namespace = '.mfp',
        addEventNames = function(pref, down, move, up, cancel) {
            mfp._tStart = pref + down + namespace;
            mfp._tMove = pref + move + namespace;
            mfp._tEnd = pref + up + namespace;
            mfp._tCancel = pref + cancel + namespace;
        };

    if(window.navigator.msPointerEnabled) {
        addEventNames('MSPointer', 'Down', 'Move', 'Up', 'Cancel');
    } else if('ontouchstart' in window) {
        addEventNames('touch', 'start', 'move', 'end', 'cancel');
    } else {
        return;
    }
    _window.on(mfp._tStart, function(e) {
        var oE = e.originalEvent;
        multipleTouches = moved = false;
        startX = oE.pageX || oE.changedTouches[0].pageX;
    }).on(mfp._tMove, function(e) {
        if(e.originalEvent.touches.length > 1) {
            multipleTouches = e.originalEvent.touches.length;
        } else {
            //e.preventDefault();
            moved = true;
        }
    }).on(mfp._tEnd + ' ' + mfp._tCancel, function(e) {
        if(moved && !multipleTouches) {
            var oE = e.originalEvent,
                diff = startX - (oE.pageX || oE.changedTouches[0].pageX);

            if(diff > 20) {
                mfp.next();
            } else if(diff < -20) {
                mfp.prev();
            }
        }
    });
},
*/


/*>>gallery*/

/*>>retina*/

var RETINA_NS = 'retina';

$.magnificPopup.registerModule(RETINA_NS, {
    options: {
        replaceSrc: function(item) {
            return item.src.replace(/\.\w+$/, function(m) { return '@2x' + m; });
        },
        ratio: 1 // Function or number.  Set to 1 to disable.
    },
    proto: {
        initRetina: function() {
            if(window.devicePixelRatio > 1) {

                var st = mfp.st.retina,
                    ratio = st.ratio;

                ratio = !isNaN(ratio) ? ratio : ratio();

                if(ratio > 1) {
                    _mfpOn('ImageHasSize' + '.' + RETINA_NS, function(e, item) {
                        item.img.css({
                            'max-width': item.img[0].naturalWidth / ratio,
                            'width': '100%'
                        });
                    });
                    _mfpOn('ElementParse' + '.' + RETINA_NS, function(e, item) {
                        item.src = st.replaceSrc(item, ratio);
                    });
                }
            }

        }
    }
});

/*>>retina*/

/*>>fastclick*/
/**
 * FastClick event implementation. (removes 300ms delay on touch devices)
 * Based on https://developers.google.com/mobile/articles/fast_buttons
 *
 * You may use it outside the Magnific Popup by calling just:
 *
 * $('.your-el').mfpFastClick(function() {
 *     console.log('Clicked!');
 * });
 *
 * To unbind:
 * $('.your-el').destroyMfpFastClick();
 * 
 * 
 * Note that it's a very basic and simple implementation, it blocks ghost click on the same element where it was bound.
 * If you need something more advanced, use plugin by FT Labs https://github.com/ftlabs/fastclick
 * 
 */

(function() {
    var ghostClickDelay = 1000,
        supportsTouch = 'ontouchstart' in window,
        unbindTouchMove = function() {
            _window.off('touchmove'+ns+' touchend'+ns);
        },
        eName = 'mfpFastClick',
        ns = '.'+eName;


    // As Zepto.js doesn't have an easy way to add custom events (like jQuery), so we implement it in this way
    $.fn.mfpFastClick = function(callback) {

        return $(this).each(function() {

            var elem = $(this),
                lock;

            if( supportsTouch ) {

                var timeout,
                    startX,
                    startY,
                    pointerMoved,
                    point,
                    numPointers;

                elem.on('touchstart' + ns, function(e) {
                    pointerMoved = false;
                    numPointers = 1;

                    point = e.originalEvent ? e.originalEvent.touches[0] : e.touches[0];
                    startX = point.clientX;
                    startY = point.clientY;

                    _window.on('touchmove'+ns, function(e) {
                        point = e.originalEvent ? e.originalEvent.touches : e.touches;
                        numPointers = point.length;
                        point = point[0];
                        if (Math.abs(point.clientX - startX) > 10 ||
                            Math.abs(point.clientY - startY) > 10) {
                            pointerMoved = true;
                            unbindTouchMove();
                        }
                    }).on('touchend'+ns, function(e) {
                        unbindTouchMove();
                        if(pointerMoved || numPointers > 1) {
                            return;
                        }
                        lock = true;
                        e.preventDefault();
                        clearTimeout(timeout);
                        timeout = setTimeout(function() {
                            lock = false;
                        }, ghostClickDelay);
                        callback();
                    });
                });

            }

            elem.on('click' + ns, function() {
                if(!lock) {
                    callback();
                }
            });
        });
    };

    $.fn.destroyMfpFastClick = function() {
        $(this).off('touchstart' + ns + ' click' + ns);
        if(supportsTouch) _window.off('touchmove'+ns+' touchend'+ns);
    };
})();

/*>>fastclick*/
 _checkInstance(); }));
/*!
 * parallax.js v1.3.1 (http://pixelcog.github.io/parallax.js/)
 * @copyright 2015 PixelCog, Inc.
 * @license MIT (https://github.com/pixelcog/parallax.js/blob/master/LICENSE)
 */
/* jshint ignore:start */

;(function ( $, window, document, undefined ) {

  // Polyfill for requestAnimationFrame
  // via: https://gist.github.com/paulirish/1579671

  (function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
      window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
      window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                                 || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
      window.requestAnimationFrame = function(callback) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() { callback(currTime + timeToCall); },
          timeToCall);
        lastTime = currTime + timeToCall;
        return id;
      };

    if (!window.cancelAnimationFrame)
      window.cancelAnimationFrame = function(id) {
        clearTimeout(id);
      };
  }());


  // Parallax Constructor

  function Parallax(element, options) {
    var self = this;

    if (typeof options == 'object') {
      delete options.refresh;
      delete options.render;
      $.extend(this, options);
    }

    this.$element = $(element);

    if (!this.imageSrc && this.$element.is('img')) {
      this.imageSrc = this.$element.attr('src');
    }

    var positions = (this.position + '').toLowerCase().match(/\S+/g) || [];

    if (positions.length < 1) {
      positions.push('center');
    }
    if (positions.length == 1) {
      positions.push(positions[0]);
    }

    if (positions[0] == 'top' || positions[0] == 'bottom' || positions[1] == 'left' || positions[1] == 'right') {
      positions = [positions[1], positions[0]];
    }

    if (this.positionX != undefined) positions[0] = this.positionX.toLowerCase();
    if (this.positionY != undefined) positions[1] = this.positionY.toLowerCase();

    self.positionX = positions[0];
    self.positionY = positions[1];

    if (this.positionX != 'left' && this.positionX != 'right') {
      if (isNaN(parseInt(this.positionX))) {
        this.positionX = 'center';
      } else {
        this.positionX = parseInt(this.positionX);
      }
    }

    if (this.positionY != 'top' && this.positionY != 'bottom') {
      if (isNaN(parseInt(this.positionY))) {
        this.positionY = 'center';
      } else {
        this.positionY = parseInt(this.positionY);
      }
    }

    this.position =
      this.positionX + (isNaN(this.positionX)? '' : 'px') + ' ' +
      this.positionY + (isNaN(this.positionY)? '' : 'px');

    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      if (this.iosFix && !this.$element.is('img')) {
        this.$element.css({
          backgroundImage: 'url(' + this.imageSrc + ')',
          backgroundSize: 'cover',
          backgroundPosition: this.position
        });
      }
      return this;
    }

    if (navigator.userAgent.match(/(Android)/)) {
      if (this.androidFix && !this.$element.is('img')) {
        this.$element.css({
          backgroundImage: 'url(' + this.imageSrc + ')',
          backgroundSize: 'cover',
          backgroundPosition: this.position
        });
      }
      return this;
    }

    this.$mirror = $('<div />').prependTo('body');
    this.$slider = $('<img />').prependTo(this.$mirror);

    this.$mirror.addClass('parallax-mirror').css({
      visibility: 'hidden',
      zIndex: this.zIndex,
      position: 'fixed',
      top: 0,
      left: 0,
      overflow: 'hidden'
    });

    this.$slider.addClass('parallax-slider').one('load', function() {
      if (!self.naturalHeight || !self.naturalWidth) {
        self.naturalHeight = this.naturalHeight || this.height || 1;
        self.naturalWidth  = this.naturalWidth  || this.width  || 1;
      }
      self.aspectRatio = self.naturalWidth / self.naturalHeight;

      Parallax.isSetup || Parallax.setup();
      Parallax.sliders.push(self);
      Parallax.isFresh = false;
      Parallax.requestRender();
    });

    this.$slider[0].src = this.imageSrc;

    if (this.naturalHeight && this.naturalWidth || this.$slider[0].complete) {
      this.$slider.trigger('load');
    }

  };


  // Parallax Instance Methods

  $.extend(Parallax.prototype, {
    speed:    0.2,
    bleed:    0,
    zIndex:   -100,
    iosFix:   true,
    androidFix: true,
    position: 'center',
    overScrollFix: false,

    refresh: function() {
      this.boxWidth        = this.$element.outerWidth();
      this.boxHeight       = this.$element.outerHeight() + this.bleed * 2;
      this.boxOffsetTop    = this.$element.offset().top - this.bleed;
      this.boxOffsetLeft   = this.$element.offset().left;
      this.boxOffsetBottom = this.boxOffsetTop + this.boxHeight;

      var winHeight = Parallax.winHeight;
      var docHeight = Parallax.docHeight;
      var maxOffset = Math.min(this.boxOffsetTop, docHeight - winHeight);
      var minOffset = Math.max(this.boxOffsetTop + this.boxHeight - winHeight, 0);
      var imageHeightMin = this.boxHeight + (maxOffset - minOffset) * (1 - this.speed) | 0;
      var imageOffsetMin = (this.boxOffsetTop - maxOffset) * (1 - this.speed) | 0;

      if (imageHeightMin * this.aspectRatio >= this.boxWidth) {
        this.imageWidth    = imageHeightMin * this.aspectRatio | 0;
        this.imageHeight   = imageHeightMin;
        this.offsetBaseTop = imageOffsetMin;

        var margin = this.imageWidth - this.boxWidth;

        if (this.positionX == 'left') {
          this.offsetLeft = 0;
        } else if (this.positionX == 'right') {
          this.offsetLeft = - margin;
        } else if (!isNaN(this.positionX)) {
          this.offsetLeft = Math.max(this.positionX, - margin);
        } else {
          this.offsetLeft = - margin / 2 | 0;
        }
      } else {
        this.imageWidth    = this.boxWidth;
        this.imageHeight   = this.boxWidth / this.aspectRatio | 0;
        this.offsetLeft    = 0;

        var margin = this.imageHeight - imageHeightMin;

        if (this.positionY == 'top') {
          this.offsetBaseTop = imageOffsetMin;
        } else if (this.positionY == 'bottom') {
          this.offsetBaseTop = imageOffsetMin - margin;
        } else if (!isNaN(this.positionY)) {
          this.offsetBaseTop = imageOffsetMin + Math.max(this.positionY, - margin);
        } else {
          this.offsetBaseTop = imageOffsetMin - margin / 2 | 0;
        }
      }
    },

    render: function() {
      var scrollTop    = Parallax.scrollTop;
      var scrollLeft   = Parallax.scrollLeft;
      var overScroll   = this.overScrollFix ? Parallax.overScroll : 0;
      var scrollBottom = scrollTop + Parallax.winHeight;

      if (this.boxOffsetBottom > scrollTop && this.boxOffsetTop < scrollBottom && this.$element.is(":visible")) {
        this.visibility = 'visible';
      } else {
        this.visibility = 'hidden';
      }
      this.mirrorTop = this.boxOffsetTop  - scrollTop;
      this.mirrorLeft = this.boxOffsetLeft - scrollLeft;
      this.offsetTop = this.offsetBaseTop - this.mirrorTop * (1 - this.speed);

      this.$mirror.css({
        transform: 'translate3d(0px, 0px, 0px)',
        visibility: this.visibility,
        top: this.mirrorTop - overScroll,
        left: this.mirrorLeft,
        height: this.boxHeight,
        width: this.boxWidth
      });

      this.$slider.css({
        transform: 'translate3d(0px, 0px, 0px)',
        position: 'absolute',
        top: this.offsetTop,
        left: this.offsetLeft,
        height: this.imageHeight,
        width: this.imageWidth,
        maxWidth: 'none'
      });
    }
  });


  // Parallax Static Methods

  $.extend(Parallax, {
    scrollTop:    0,
    scrollLeft:   0,
    winHeight:    0,
    winWidth:     0,
    docHeight:    1 << 30,
    docWidth:     1 << 30,
    sliders:      [],
    isReady:      false,
    isFresh:      false,
    isBusy:       false,

    setup: function() {
      if (this.isReady) return;

      var $doc = $(document), $win = $(window);

      $win.on('scroll.px.parallax load.px.parallax', function() {
          var scrollTopMax  = Parallax.docHeight - Parallax.winHeight;
          var scrollLeftMax = Parallax.docWidth  - Parallax.winWidth;
          Parallax.scrollTop  = Math.max(0, Math.min(scrollTopMax,  $win.scrollTop()));
          Parallax.scrollLeft = Math.max(0, Math.min(scrollLeftMax, $win.scrollLeft()));
          Parallax.overScroll = Math.max($win.scrollTop() - scrollTopMax, Math.min($win.scrollTop(), 0));
          Parallax.requestRender();
        })
        .on('resize.px.parallax load.px.parallax', function() {
          Parallax.winHeight = $win.height();
          Parallax.winWidth  = $win.width();
          Parallax.docHeight = $doc.height();
          Parallax.docWidth  = $doc.width();
          Parallax.isFresh = false;
          Parallax.requestRender();
        });

      this.isReady = true;
    },

    configure: function(options) {
      if (typeof options == 'object') {
        delete options.refresh;
        delete options.render;
        $.extend(this.prototype, options);
      }
    },

    refresh: function() {
      $.each(this.sliders, function(){ this.refresh() });
      this.isFresh = true;
    },

    render: function() {
      this.isFresh || this.refresh();
      $.each(this.sliders, function(){ this.render() });
    },

    requestRender: function() {
      var self = this;

      if (!this.isBusy) {
        this.isBusy = true;
        window.requestAnimationFrame(function() {
          self.render();
          self.isBusy = false;
        });
      }
    }
  });


  // Parallax Plugin Definition

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this);
      var options = typeof option == 'object' && option;

      if (this == window || this == document || $this.is('body')) {
        Parallax.configure(options);
      }
      else if (!$this.data('px.parallax')) {
        options = $.extend({}, $this.data(), options);
        $this.data('px.parallax', new Parallax(this, options));
      }
      if (typeof option == 'string') {
        Parallax[option]();
      }
    })
  };

  var old = $.fn.parallax;

  $.fn.parallax             = Plugin;
  $.fn.parallax.Constructor = Parallax;


  // Parallax No Conflict

  $.fn.parallax.noConflict = function () {
    $.fn.parallax = old;
    return this;
  };


  // Parallax Data-API

  $(document).on('ready.px.parallax.data-api', function () {
    $('[data-parallax="scroll"]').parallax();
  });

}(jQuery, window, document));

$.fn.real_height = function (natural_mobile, on_set) {
    "use strict";
    var measured = "measured";
    if (typeof on_set === "undefined") {
        on_set = function () { return null; };
    }
    this.each(function () {
        var el = $(this),
            set_height = function () {
                var height;
                el
                    .removeClass(measured)
                    .css("height", "");
                height = el.outerHeight();
                el
                    .height(height)
                    .addClass(measured);
                on_set(height, el);
            };

        if (natural_mobile) {
            el.addClass("natural-mobile");
        }
        el.load(set_height());
        $(window)
            .load(set_height)
            .resize(set_height);
    });

    return this;
};
$.fn.tabify = function (options) {
    "use strict";
    if (this.length > 0) {
        var defaults = {
                tab:                ".tabs__tab",
                body:               ".tab-body",
                tab_active:         "tabs__tab--active",
                body_active:        "tab-body--active",
                init_class:         "tabbed-section--active",
                first_tab:          false,
                equalize_height:    false,
                split_tab_width:    false,
                on_change:          function () { return null; }
            },
            wrapper =       this,
            settings =      $.extend({}, defaults, options),
            tabs =          wrapper.find(settings.tab),
            tab_bodies =    wrapper.find(settings.body),
            find_body = function (tab) {
                var selector = tab.attr("href") || tab.data("target") || tab.find("a").attr("href"),
                    body = $(selector);

                if (body.length === 1) {
                    return body;
                }
                console.log("Invalid tab body. Your selector was " + selector);
                return false;
            },
            find_tab = function (body) {
                var id = body.attr("id");
                return tabs.filter(function () {
                    if ($(this).is("[href=#" + id + "], [data-target=#" + id + "]") || $(this).find("a").is("[href=#" + id + "]")) {
                        return true;
                    }
                    return false;
                });
            },
            change_tab = function (new_tab, push) {
                var new_body = find_body(new_tab);
                if (push === undefined) {
                    push = true;
                }
                if (new_body !== false) {
                    tabs.removeClass(settings.tab_active);
                    tab_bodies.removeClass(settings.body_active);
                    new_tab.addClass(settings.tab_active);
                    new_body.addClass(settings.body_active);

                    if (push && history.pushState) {
                        history.pushState(null, null, "#" + new_body.attr("id"));
                    }

                    settings.on_change(new_tab, new_body);
                }
            },
            initial_tab = function () {
                var hash_target_tab = tab_bodies.filter(location.hash),
                    hash_target_tab_body = tab_bodies.find(location.hash).closest(settings.body);
                if (location.hash && hash_target_tab.length > 0) {
                    return find_tab(hash_target_tab);
                }
                if (location.hash && hash_target_tab_body.length > 0) {
                    return find_tab(hash_target_tab_body);
                }
                if (settings.first_tab) {
                    return settings.first_tab;
                }
                return tabs.first();
            },
            split_tab_width = function () {
                if (settings.split_tab_width) {
                    var total_width = wrapper.find(settings.split_tab_width).width();
                    tabs.outerWidth(Math.floor(total_width / tabs.length));
                }
            };

        if (tab_bodies.length > 1) {
            split_tab_width();
            wrapper.addClass(settings.init_class);
            change_tab(initial_tab(), false);

            if (settings.equalize_height) {
                tab_bodies.equal_height();
            }

            tabs
                .click(function (e) {
                    e.preventDefault();
                    change_tab($(this), true);
                })
                .add(tabs.find("a"))
                .data("no_scroll", true);

            tab_bodies
                .each(function () {
                    var body = $(this),
                        anchors = $("a[href*=#" + body.attr("id") + "]").not(tabs);

                    anchors
                        .data("no_scroll", true)
                        .click(function (e) {
                            e.preventDefault();
                            change_tab(find_tab(body), true);
                        });
                });

            $(window)
                .resize(function () {
                    split_tab_width();
                });
        } else if (tab_bodies.length === 1) {
            tabs.closest(".widget-span").hide();
        } else {
            $(this).addClass("tabbed-section--empty");
        }
    }
    return this;
};
/*********************************************************************
*  #### Twitter Post Fetcher v13.0 ####
*  Coded by Jason Mayes 2015. A present to all the developers out there.
*  www.jasonmayes.com
*  Please keep this disclaimer with my code if you use it. Thanks. :-)
*  Got feedback or questions, ask here:
*  http://www.jasonmayes.com/projects/twitterApi/
*  Github: https://github.com/jasonmayes/Twitter-Post-Fetcher
*  Updates will be posted to this site.
*********************************************************************/
/* jshint ignore:start */
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define([], factory);
  } else if (typeof exports === 'object') {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory();
  } else {
    // Browser globals.
    factory();
  }
}(this, function() {
  var domNode = '';
  var maxTweets = 20;
  var parseLinks = true;
  var queue = [];
  var inProgress = false;
  var printTime = true;
  var printUser = true;
  var formatterFunction = null;
  var supportsClassName = true;
  var showRts = true;
  var customCallbackFunction = null;
  var showInteractionLinks = true;
  var showImages = false;
  var targetBlank = true;
  var lang = 'en';

  function handleTweets(tweets){
    if (customCallbackFunction === null) {
      var x = tweets.length;
      var n = 0;
      var element = document.getElementById(domNode);
      var html = '<ul>';
      while(n < x) {
        html += '<li>' + tweets[n] + '</li>';
        n++;
      }
      html += '</ul>';
      element.innerHTML = html;
    } else {
      customCallbackFunction(tweets);
    }
  }

  function strip(data) {
    return data.replace(/<b[^>]*>(.*?)<\/b>/gi, function(a,s){return s;})
        .replace(/class=".*?"|data-query-source=".*?"|dir=".*?"|rel=".*?"/gi,
        '');
  }

  function targetLinksToNewWindow(el) {
    var links = el.getElementsByTagName('a');
    for (var i = links.length - 1; i >= 0; i--) {
      links[i].setAttribute('target', '_blank');
    }
  }

  function getElementsByClassName (node, classname) {
    var a = [];
    var regex = new RegExp('(^| )' + classname + '( |$)');
    var elems = node.getElementsByTagName('*');
    for (var i = 0, j = elems.length; i < j; i++) {
        if(regex.test(elems[i].className)){
          a.push(elems[i]);
        }
    }
    return a;
  }

  function extractImageUrl(image_data) {
    if (image_data !== undefined) {
      var data_src = image_data.innerHTML.match(/data-srcset="([A-z0-9%_\.-]+)/i)[0];
      return decodeURIComponent(data_src).split('"')[1];
    }
  }

  var twitterFetcher = {
    fetch: function(config) {
      if (config.maxTweets === undefined) {
        config.maxTweets = 20;
      }
      if (config.enableLinks === undefined) {
        config.enableLinks = true;
      }
      if (config.showUser === undefined) {
        config.showUser = true;
      }
      if (config.showTime === undefined) {
        config.showTime = true;
      }
      if (config.dateFunction === undefined) {
        config.dateFunction = 'default';
      }
      if (config.showRetweet === undefined) {
        config.showRetweet = true;
      }
      if (config.customCallback === undefined) {
        config.customCallback = null;
      }
      if (config.showInteraction === undefined) {
        config.showInteraction = true;
      }
      if (config.showImages === undefined) {
        config.showImages = false;
      }
      if (config.linksInNewWindow === undefined) {
        config.linksInNewWindow = true;
      }

      if (inProgress) {
        queue.push(config);
      } else {
        inProgress = true;

        domNode = config.domId;
        maxTweets = config.maxTweets;
        parseLinks = config.enableLinks;
        printUser = config.showUser;
        printTime = config.showTime;
        showRts = config.showRetweet;
        formatterFunction = config.dateFunction;
        customCallbackFunction = config.customCallback;
        showInteractionLinks = config.showInteraction;
        showImages = config.showImages;
        targetBlank = config.linksInNewWindow;

        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = '//cdn.syndication.twimg.com/widgets/timelines/' +
            config.id + '?&lang=' + (config.lang || lang) + '&callback=twitterFetcher.callback&' +
            'suppress_response_codes=true&rnd=' + Math.random();
        document.getElementsByTagName('head')[0].appendChild(script);
      }
    },
    callback: function(data) {
      var div = document.createElement('div');
      div.innerHTML = data.body;
      if (typeof(div.getElementsByClassName) === 'undefined') {
         supportsClassName = false;
      }

      var tweets = [];
      var authors = [];
      var times = [];
      var images = [];
      var rts = [];
      var tids = [];
      var x = 0;

      if (supportsClassName) {
        var tmp = div.getElementsByClassName('timeline-Tweet');
        while (x < tmp.length) {
          if (tmp[x].getElementsByClassName('timeline-Tweet-retweetCredit').length > 0) {
            rts.push(true);
          } else {
            rts.push(false);
          }
          if (!rts[x] || rts[x] && showRts) {
            tweets.push(tmp[x].getElementsByClassName('timeline-Tweet-text')[0]);
            tids.push(tmp[x].getAttribute('data-tweet-id'));
            authors.push(tmp[x].getElementsByClassName('timeline-Tweet-author')[0]);
            times.push(tmp[x].getElementsByClassName('dt-updated')[0]);
            if (tmp[x].getElementsByClassName('timeline-Tweet-media')[0] !== undefined) {
              images.push(tmp[x].getElementsByClassName('timeline-Tweet-media')[0]);
            } else {
              images.push(undefined);
            }
          }
          x++;
        }
      } else {
        var tmp = getElementsByClassName(div, 'timeline-Tweet');
        while (x < tmp.length) {
          tweets.push(getElementsByClassName(tmp[x], 'timeline-Tweet-text')[0]);
          tids.push(tmp[x].getAttribute('data-tweet-id'));
          authors.push(getElementsByClassName(tmp[x], 'timeline-Tweet-author')[0]);
          times.push(getElementsByClassName(tmp[x], 'dt-updated')[0]);
          if (getElementsByClassName(tmp[x], 'timeline-Tweet-media')[0] !== undefined) {
            images.push(getElementsByClassName(tmp[x], 'timeline-Tweet-media')[0]);
          } else {
            images.push(undefined);
          }

          if (getElementsByClassName(tmp[x], 'timeline-Tweet-retweetCredit').length > 0) {
            rts.push(true);
          } else {
            rts.push(false);
          }
          x++;
        }
      }

      if (tweets.length > maxTweets) {
        tweets.splice(maxTweets, (tweets.length - maxTweets));
        authors.splice(maxTweets, (authors.length - maxTweets));
        times.splice(maxTweets, (times.length - maxTweets));
        rts.splice(maxTweets, (rts.length - maxTweets));
        images.splice(maxTweets, (images.length - maxTweets));
      }

      var arrayTweets = [];
      var x = tweets.length;
      var n = 0;
      while(n < x) {
        if (typeof(formatterFunction) !== 'string') {
          var datetimeText = times[n].getAttribute('datetime');
          var newDate = new Date(times[n].getAttribute('datetime')
              .replace(/-/g,'/').replace('T', ' ').split('+')[0]);
          var dateString = formatterFunction(newDate, datetimeText);
          times[n].setAttribute('aria-label', dateString);

          if (tweets[n].innerText) {
            // IE hack.
            if (supportsClassName) {
              times[n].innerText = dateString;
            } else {
              var h = document.createElement('p');
              var t = document.createTextNode(dateString);
              h.appendChild(t);
              h.setAttribute('aria-label', dateString);
              times[n] = h;
            }
          } else {
            times[n].textContent = dateString;
          }
        }
        var op = '';
        if (parseLinks) {
          if (targetBlank) {
            targetLinksToNewWindow(tweets[n]);
            if (printUser) {
              targetLinksToNewWindow(authors[n]);
            }
          }
          if (printUser) {
            op += '<div class="user">' + strip(authors[n].innerHTML) +
                '</div>';
          }
          op += '<p class="tweet">' + strip(tweets[n].innerHTML) + '</p>';
          if (printTime) {
            op += '<p class="timePosted">' +
                times[n].getAttribute('aria-label') + '</p>';
          }
        } else {
          if (tweets[n].innerText) {
            if (printUser) {
              op += '<p class="user">' + authors[n].innerText + '</p>';
            }
            op += '<p class="tweet">' +  tweets[n].innerText + '</p>';
            if (printTime) {
              op += '<p class="timePosted">' + times[n].innerText + '</p>';
            }

          } else {
            if (printUser) {
              op += '<p class="user">' + authors[n].textContent + '</p>';
            }
            op += '<p class="tweet">' +  tweets[n].textContent + '</p>';
            if (printTime) {
              op += '<p class="timePosted">' + times[n].textContent + '</p>';
            }
          }
        }
        if (showInteractionLinks) {
          op += '<p class="interact"><a href="https://twitter.com/intent/' +
              'tweet?in_reply_to=' + tids[n] + '" class="twitter_reply_icon"' + (targetBlank ? ' target="_blank">' : '>') +
              'Reply</a><a href="https://twitter.com/intent/retweet?tweet_id=' +
              tids[n] + '" class="twitter_retweet_icon"' + (targetBlank ? ' target="_blank">' : '>') + 'Retweet</a>' +
              '<a href="https://twitter.com/intent/favorite?tweet_id=' +
              tids[n] + '" class="twitter_fav_icon"' + (targetBlank ? ' target="_blank">' : '>') + 'Favorite</a></p>';
        }

        if (showImages && images[n] !== undefined) {
          op += '<div class="media">' +
              '<img src="' + extractImageUrl(images[n]) + '" alt="Image from tweet" />' +
              '</div>';
        }

        arrayTweets.push(op);
        n++;
      }
      handleTweets(arrayTweets);
      inProgress = false;

      if (queue.length > 0) {
        twitterFetcher.fetch(queue[0]);
        queue.splice(0,1);
      }
    }
  };

  // It must be a global variable because it will be called by JSONP.
  window.twitterFetcher = twitterFetcher;

  return twitterFetcher;
}));
// Generated by CoffeeScript 1.6.2
/*!
jQuery Waypoints - v2.0.5
Copyright (c) 2011-2014 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/jquery-waypoints/blob/master/licenses.txt
*/
/* jshint ignore:start */


(function() {
  var __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; },
    __slice = [].slice;

  (function(root, factory) {
    if (typeof define === 'function' && define.amd) {
      return define('waypoints', ['jquery'], function($) {
        return factory($, root);
      });
    } else {
      return factory(root.jQuery, root);
    }
  })(window, function($, window) {
    var $w, Context, Waypoint, allWaypoints, contextCounter, contextKey, contexts, isTouch, jQMethods, methods, resizeEvent, scrollEvent, waypointCounter, waypointKey, wp, wps;

    $w = $(window);
    isTouch = __indexOf.call(window, 'ontouchstart') >= 0;
    allWaypoints = {
      horizontal: {},
      vertical: {}
    };
    contextCounter = 1;
    contexts = {};
    contextKey = 'waypoints-context-id';
    resizeEvent = 'resize.waypoints';
    scrollEvent = 'scroll.waypoints';
    waypointCounter = 1;
    waypointKey = 'waypoints-waypoint-ids';
    wp = 'waypoint';
    wps = 'waypoints';
    Context = (function() {
      function Context($element) {
        var _this = this;

        this.$element = $element;
        this.element = $element[0];
        this.didResize = false;
        this.didScroll = false;
        this.id = 'context' + contextCounter++;
        this.oldScroll = {
          x: $element.scrollLeft(),
          y: $element.scrollTop()
        };
        this.waypoints = {
          horizontal: {},
          vertical: {}
        };
        this.element[contextKey] = this.id;
        contexts[this.id] = this;
        $element.bind(scrollEvent, function() {
          var scrollHandler;

          if (!(_this.didScroll || isTouch)) {
            _this.didScroll = true;
            scrollHandler = function() {
              _this.doScroll();
              return _this.didScroll = false;
            };
            return window.setTimeout(scrollHandler, $[wps].settings.scrollThrottle);
          }
        });
        $element.bind(resizeEvent, function() {
          var resizeHandler;

          if (!_this.didResize) {
            _this.didResize = true;
            resizeHandler = function() {
              $[wps]('refresh');
              return _this.didResize = false;
            };
            return window.setTimeout(resizeHandler, $[wps].settings.resizeThrottle);
          }
        });
      }

      Context.prototype.doScroll = function() {
        var axes,
          _this = this;

        axes = {
          horizontal: {
            newScroll: this.$element.scrollLeft(),
            oldScroll: this.oldScroll.x,
            forward: 'right',
            backward: 'left'
          },
          vertical: {
            newScroll: this.$element.scrollTop(),
            oldScroll: this.oldScroll.y,
            forward: 'down',
            backward: 'up'
          }
        };
        if (isTouch && (!axes.vertical.oldScroll || !axes.vertical.newScroll)) {
          $[wps]('refresh');
        }
        $.each(axes, function(aKey, axis) {
          var direction, isForward, triggered;

          triggered = [];
          isForward = axis.newScroll > axis.oldScroll;
          direction = isForward ? axis.forward : axis.backward;
          $.each(_this.waypoints[aKey], function(wKey, waypoint) {
            var _ref, _ref1;

            if ((axis.oldScroll < (_ref = waypoint.offset) && _ref <= axis.newScroll)) {
              return triggered.push(waypoint);
            } else if ((axis.newScroll < (_ref1 = waypoint.offset) && _ref1 <= axis.oldScroll)) {
              return triggered.push(waypoint);
            }
          });
          triggered.sort(function(a, b) {
            return a.offset - b.offset;
          });
          if (!isForward) {
            triggered.reverse();
          }
          return $.each(triggered, function(i, waypoint) {
            if (waypoint.options.continuous || i === triggered.length - 1) {
              return waypoint.trigger([direction]);
            }
          });
        });
        return this.oldScroll = {
          x: axes.horizontal.newScroll,
          y: axes.vertical.newScroll
        };
      };

      Context.prototype.refresh = function() {
        var axes, cOffset, isWin,
          _this = this;

        isWin = $.isWindow(this.element);
        cOffset = this.$element.offset();
        this.doScroll();
        axes = {
          horizontal: {
            contextOffset: isWin ? 0 : cOffset.left,
            contextScroll: isWin ? 0 : this.oldScroll.x,
            contextDimension: this.$element.width(),
            oldScroll: this.oldScroll.x,
            forward: 'right',
            backward: 'left',
            offsetProp: 'left'
          },
          vertical: {
            contextOffset: isWin ? 0 : cOffset.top,
            contextScroll: isWin ? 0 : this.oldScroll.y,
            contextDimension: isWin ? $[wps]('viewportHeight') : this.$element.height(),
            oldScroll: this.oldScroll.y,
            forward: 'down',
            backward: 'up',
            offsetProp: 'top'
          }
        };
        return $.each(axes, function(aKey, axis) {
          return $.each(_this.waypoints[aKey], function(i, waypoint) {
            var adjustment, elementOffset, oldOffset, _ref, _ref1;

            adjustment = waypoint.options.offset;
            oldOffset = waypoint.offset;
            elementOffset = $.isWindow(waypoint.element) ? 0 : waypoint.$element.offset()[axis.offsetProp];
            if ($.isFunction(adjustment)) {
              adjustment = adjustment.apply(waypoint.element);
            } else if (typeof adjustment === 'string') {
              adjustment = parseFloat(adjustment);
              if (waypoint.options.offset.indexOf('%') > -1) {
                adjustment = Math.ceil(axis.contextDimension * adjustment / 100);
              }
            }
            waypoint.offset = elementOffset - axis.contextOffset + axis.contextScroll - adjustment;
            if ((waypoint.options.onlyOnScroll && (oldOffset != null)) || !waypoint.enabled) {
              return;
            }
            if (oldOffset !== null && (oldOffset < (_ref = axis.oldScroll) && _ref <= waypoint.offset)) {
              return waypoint.trigger([axis.backward]);
            } else if (oldOffset !== null && (oldOffset > (_ref1 = axis.oldScroll) && _ref1 >= waypoint.offset)) {
              return waypoint.trigger([axis.forward]);
            } else if (oldOffset === null && axis.oldScroll >= waypoint.offset) {
              return waypoint.trigger([axis.forward]);
            }
          });
        });
      };

      Context.prototype.checkEmpty = function() {
        if ($.isEmptyObject(this.waypoints.horizontal) && $.isEmptyObject(this.waypoints.vertical)) {
          this.$element.unbind([resizeEvent, scrollEvent].join(' '));
          return delete contexts[this.id];
        }
      };

      return Context;

    })();
    Waypoint = (function() {
      function Waypoint($element, context, options) {
        var idList, _ref;

        if (options.offset === 'bottom-in-view') {
          options.offset = function() {
            var contextHeight;

            contextHeight = $[wps]('viewportHeight');
            if (!$.isWindow(context.element)) {
              contextHeight = context.$element.height();
            }
            return contextHeight - $(this).outerHeight();
          };
        }
        this.$element = $element;
        this.element = $element[0];
        this.axis = options.horizontal ? 'horizontal' : 'vertical';
        this.callback = options.handler;
        this.context = context;
        this.enabled = options.enabled;
        this.id = 'waypoints' + waypointCounter++;
        this.offset = null;
        this.options = options;
        context.waypoints[this.axis][this.id] = this;
        allWaypoints[this.axis][this.id] = this;
        idList = (_ref = this.element[waypointKey]) != null ? _ref : [];
        idList.push(this.id);
        this.element[waypointKey] = idList;
      }

      Waypoint.prototype.trigger = function(args) {
        if (!this.enabled) {
          return;
        }
        if (this.callback != null) {
          this.callback.apply(this.element, args);
        }
        if (this.options.triggerOnce) {
          return this.destroy();
        }
      };

      Waypoint.prototype.disable = function() {
        return this.enabled = false;
      };

      Waypoint.prototype.enable = function() {
        this.context.refresh();
        return this.enabled = true;
      };

      Waypoint.prototype.destroy = function() {
        delete allWaypoints[this.axis][this.id];
        delete this.context.waypoints[this.axis][this.id];
        return this.context.checkEmpty();
      };

      Waypoint.getWaypointsByElement = function(element) {
        var all, ids;

        ids = element[waypointKey];
        if (!ids) {
          return [];
        }
        all = $.extend({}, allWaypoints.horizontal, allWaypoints.vertical);
        return $.map(ids, function(id) {
          return all[id];
        });
      };

      return Waypoint;

    })();
    methods = {
      init: function(f, options) {
        var _ref;

        options = $.extend({}, $.fn[wp].defaults, options);
        if ((_ref = options.handler) == null) {
          options.handler = f;
        }
        this.each(function() {
          var $this, context, contextElement, _ref1;

          $this = $(this);
          contextElement = (_ref1 = options.context) != null ? _ref1 : $.fn[wp].defaults.context;
          if (!$.isWindow(contextElement)) {
            contextElement = $this.closest(contextElement);
          }
          contextElement = $(contextElement);
          context = contexts[contextElement[0][contextKey]];
          if (!context) {
            context = new Context(contextElement);
          }
          return new Waypoint($this, context, options);
        });
        $[wps]('refresh');
        return this;
      },
      disable: function() {
        return methods._invoke.call(this, 'disable');
      },
      enable: function() {
        return methods._invoke.call(this, 'enable');
      },
      destroy: function() {
        return methods._invoke.call(this, 'destroy');
      },
      prev: function(axis, selector) {
        return methods._traverse.call(this, axis, selector, function(stack, index, waypoints) {
          if (index > 0) {
            return stack.push(waypoints[index - 1]);
          }
        });
      },
      next: function(axis, selector) {
        return methods._traverse.call(this, axis, selector, function(stack, index, waypoints) {
          if (index < waypoints.length - 1) {
            return stack.push(waypoints[index + 1]);
          }
        });
      },
      _traverse: function(axis, selector, push) {
        var stack, waypoints;

        if (axis == null) {
          axis = 'vertical';
        }
        if (selector == null) {
          selector = window;
        }
        waypoints = jQMethods.aggregate(selector);
        stack = [];
        this.each(function() {
          var index;

          index = $.inArray(this, waypoints[axis]);
          return push(stack, index, waypoints[axis]);
        });
        return this.pushStack(stack);
      },
      _invoke: function(method) {
        this.each(function() {
          var waypoints;

          waypoints = Waypoint.getWaypointsByElement(this);
          return $.each(waypoints, function(i, waypoint) {
            waypoint[method]();
            return true;
          });
        });
        return this;
      }
    };
    $.fn[wp] = function() {
      var args, method;

      method = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      if (methods[method]) {
        return methods[method].apply(this, args);
      } else if ($.isFunction(method)) {
        return methods.init.apply(this, arguments);
      } else if ($.isPlainObject(method)) {
        return methods.init.apply(this, [null, method]);
      } else if (!method) {
        return $.error("jQuery Waypoints needs a callback function or handler option.");
      } else {
        return $.error("The " + method + " method does not exist in jQuery Waypoints.");
      }
    };
    $.fn[wp].defaults = {
      context: window,
      continuous: true,
      enabled: true,
      horizontal: false,
      offset: 0,
      triggerOnce: false
    };
    jQMethods = {
      refresh: function() {
        return $.each(contexts, function(i, context) {
          return context.refresh();
        });
      },
      viewportHeight: function() {
        var _ref;

        return (_ref = window.innerHeight) != null ? _ref : $w.height();
      },
      aggregate: function(contextSelector) {
        var collection, waypoints, _ref;

        collection = allWaypoints;
        if (contextSelector) {
          collection = (_ref = contexts[$(contextSelector)[0][contextKey]]) != null ? _ref.waypoints : void 0;
        }
        if (!collection) {
          return [];
        }
        waypoints = {
          horizontal: [],
          vertical: []
        };
        $.each(waypoints, function(axis, arr) {
          $.each(collection[axis], function(key, waypoint) {
            return arr.push(waypoint);
          });
          arr.sort(function(a, b) {
            return a.offset - b.offset;
          });
          waypoints[axis] = $.map(arr, function(waypoint) {
            return waypoint.element;
          });
          return waypoints[axis] = $.unique(waypoints[axis]);
        });
        return waypoints;
      },
      above: function(contextSelector) {
        if (contextSelector == null) {
          contextSelector = window;
        }
        return jQMethods._filter(contextSelector, 'vertical', function(context, waypoint) {
          return waypoint.offset <= context.oldScroll.y;
        });
      },
      below: function(contextSelector) {
        if (contextSelector == null) {
          contextSelector = window;
        }
        return jQMethods._filter(contextSelector, 'vertical', function(context, waypoint) {
          return waypoint.offset > context.oldScroll.y;
        });
      },
      left: function(contextSelector) {
        if (contextSelector == null) {
          contextSelector = window;
        }
        return jQMethods._filter(contextSelector, 'horizontal', function(context, waypoint) {
          return waypoint.offset <= context.oldScroll.x;
        });
      },
      right: function(contextSelector) {
        if (contextSelector == null) {
          contextSelector = window;
        }
        return jQMethods._filter(contextSelector, 'horizontal', function(context, waypoint) {
          return waypoint.offset > context.oldScroll.x;
        });
      },
      enable: function() {
        return jQMethods._invoke('enable');
      },
      disable: function() {
        return jQMethods._invoke('disable');
      },
      destroy: function() {
        return jQMethods._invoke('destroy');
      },
      extendFn: function(methodName, f) {
        return methods[methodName] = f;
      },
      _invoke: function(method) {
        var waypoints;

        waypoints = $.extend({}, allWaypoints.vertical, allWaypoints.horizontal);
        return $.each(waypoints, function(key, waypoint) {
          waypoint[method]();
          return true;
        });
      },
      _filter: function(selector, axis, test) {
        var context, waypoints;

        context = contexts[$(selector)[0][contextKey]];
        if (!context) {
          return [];
        }
        waypoints = [];
        $.each(context.waypoints[axis], function(i, waypoint) {
          if (test(context, waypoint)) {
            return waypoints.push(waypoint);
          }
        });
        waypoints.sort(function(a, b) {
          return a.offset - b.offset;
        });
        return $.map(waypoints, function(waypoint) {
          return waypoint.element;
        });
      }
    };
    $[wps] = function() {
      var args, method;

      method = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      if (jQMethods[method]) {
        return jQMethods[method].apply(null, args);
      } else {
        return jQMethods.aggregate.call(null, method);
      }
    };
    $[wps].settings = {
      resizeThrottle: 100,
      scrollThrottle: 30
    };
    return $w.on('load.waypoints', function() {
      return $[wps]('refresh');
    });
  });

}).call(this);