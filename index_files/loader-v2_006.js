(function() {

    var __hs_cta_json = {"css":"a#cta_button_338227_04db3bcd-a545-42a1-a2e4-231ba4d02888 {\n  cursor:pointer; \ncolor: #2A919D; font-weight: bold; text-transform: uppercase; text-decoration: none;}\na#cta_button_338227_04db3bcd-a545-42a1-a2e4-231ba4d02888:hover {\n}\na#cta_button_338227_04db3bcd-a545-42a1-a2e4-231ba4d02888:active, #cta_button_338227_04db3bcd-a545-42a1-a2e4-231ba4d02888:active:hover {\n}\n\n","image":false,"_id":"false","raw_html":"<a id=\"cta_button_338227_04db3bcd-a545-42a1-a2e4-231ba4d02888\" class=\"cta_button cta cta--myiclub\" href=\"//cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=04db3bcd-a545-42a1-a2e4-231ba4d02888&placement_guid=936ec653-292e-47e7-8d54-4287ef1f478a&portal_id=338227&redirect_url=APefjpEokLf8HwOTvkphO2ynxFj_VfLDG4az6Vznmtotsvs7tcPkTlVVitxDh0b3DMQDBtPtaKhb-p1ryEHUct3t7LVe-IOE3qFnw-TYbCn1Pd1XpmX_-UKplxqfuGuqhQIhPsfPyIO5PxzBAyoHFcA56jzyUa9PeLIsbeEpUpkyGkUYLYvN52U&hsutk=&canon=http%3A%2F%2Fwww.californiafamilyfitness.com%2F\" target=\"_blank\" style=\"/*hs-extra-styles*/\" cta_dest_link=\"https://mico.myiclubonline.com/iclub/members/signin.htm?clubNumber=0445#signin\" title=\"MYiCLUB\"> <span>MYiCLUB</span> </a> ","image_html":"<a id=\"cta_button_338227_04db3bcd-a545-42a1-a2e4-231ba4d02888\" class=\"cta_button\" href=\"//cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=04db3bcd-a545-42a1-a2e4-231ba4d02888&placement_guid=936ec653-292e-47e7-8d54-4287ef1f478a&portal_id=338227&redirect_url=APefjpEokLf8HwOTvkphO2ynxFj_VfLDG4az6Vznmtotsvs7tcPkTlVVitxDh0b3DMQDBtPtaKhb-p1ryEHUct3t7LVe-IOE3qFnw-TYbCn1Pd1XpmX_-UKplxqfuGuqhQIhPsfPyIO5PxzBAyoHFcA56jzyUa9PeLIsbeEpUpkyGkUYLYvN52U&hsutk=&canon=http%3A%2F%2Fwww.californiafamilyfitness.com%2F\"  target=\"_blank\"  cta_dest_link=\"https://mico.myiclubonline.com/iclub/members/signin.htm?clubNumber=0445#signin\"> <img id=\"hs-cta-img-936ec653-292e-47e7-8d54-4287ef1f478a\" class=\"hs-cta-img cta cta--myiclub\" style=\"border-width: 0px; /*hs-extra-styles*/\" mce_noresize=\"1\" alt=\"MYiCLUB\" src=\"http://cdn2.hubspot.net/hubshot/16/03/11/802a7bd9-f09b-4a1a-a0e1-8217efee77c6.png\" /> </a> ","is_image":false,"_class":"hs-cta-936ec653-292e-47e7-8d54-4287ef1f478a"};
    var __hs_cta = {};

    __hs_cta.drop = function() {
        var is_legacy = document.getElementById('hs-cta-ie-element') && true || false,
            html = __hs_cta_json.image_html,
            tags = __hs_cta.getTags(),
            is_image = __hs_cta_json.is_image,
            parent, _style;

        if (!is_legacy && !is_image) {
            parent = (document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]);

            _style = document.createElement('style');
            parent.insertBefore(_style, parent.childNodes[0]);
            try {
                default_css = ".hs-cta-wrapper p, .hs-cta-wrapper div { margin: 0; padding: 0; }";
                cta_css = default_css + " " + __hs_cta_json.css;
                // http://blog.coderlab.us/2005/09/22/using-the-innertext-property-with-firefox/
                _style[document.all ? 'innerText' : 'textContent'] = cta_css;
            } catch (e) {
                // addressing an ie9 issue
                _style.styleSheet.cssText = cta_css;
            }

            html = __hs_cta_json.raw_html;
        }

        for (var i = 0; i < tags.length; ++i) {

            var tag = tags[i];
            var images = tag.getElementsByTagName('img');
            var cssText = "";
            var align = "";
            for (var j = 0; j < images.length; j++) {
                align = images[j].align;
                images[j].style.border = '';
                images[j].style.display = '';
                cssText = images[j].style.cssText;
            }

            if (align == "right") {
                tag.style.display = "block";
                tag.style.textAlign = "right";
            } else if (align == "middle") {
                tag.style.display = "block";
                tag.style.textAlign = "center";
            }

            tag.innerHTML = html.replace('/*hs-extra-styles*/', cssText);
            tag.style.visibility = 'visible';
            tag.setAttribute('data-hs-drop', 'true');
            window.hbspt && hbspt.cta && hbspt.cta.afterLoad && hbspt.cta.afterLoad();
        }

        return tags;
    };

    __hs_cta.getTags = function() {
        var allTags, check, i, divTags, spanTags,
            tags = [];
            if (document.getElementsByClassName) {
                allTags = document.getElementsByClassName(__hs_cta_json._class);
                check = function(ele) {
                    return (ele.nodeName == 'DIV' || ele.nodeName == 'SPAN') && (!ele.getAttribute('data-hs-drop'));
                };
            } else {
                allTags = [];
                divTags = document.getElementsByTagName("div");
                spanTags = document.getElementsByTagName("span");
                for (i = 0; i < spanTags.length; i++) {
                    allTags.push(spanTags[i]);
                }
                for (i = 0; i < divTags.length; i++) {
                    allTags.push(divTags[i]);
                }

                check = function(ele) {
                    return (ele.className.indexOf(__hs_cta_json._class) > -1) && (!ele.getAttribute('data-hs-drop'));
                };
            }
            for (i = 0; i < allTags.length; ++i) {
                if (check(allTags[i])) {
                    tags.push(allTags[i]);
                }
            }
        return tags;
    };

    // need to do a slight timeout so IE has time to react
    setTimeout(__hs_cta.drop, 10);
}());
