(function() {

    var __hs_cta_json = {"css":"a#cta_button_338227_6bb10ef3-a45e-4994-bcd4-3e74335ec2c7 {\n  cursor:pointer !important; \n}\na#cta_button_338227_6bb10ef3-a45e-4994-bcd4-3e74335ec2c7:hover {\n}\na#cta_button_338227_6bb10ef3-a45e-4994-bcd4-3e74335ec2c7:active, #cta_button_338227_6bb10ef3-a45e-4994-bcd4-3e74335ec2c7:active:hover {\n}\n\n","image":false,"_id":"false","raw_html":"<a id=\"cta_button_338227_6bb10ef3-a45e-4994-bcd4-3e74335ec2c7\" class=\"cta_button \" href=\"//cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=6bb10ef3-a45e-4994-bcd4-3e74335ec2c7&placement_guid=d6c5389a-b3a5-4daa-b6c9-ba82ec68e91a&portal_id=338227&redirect_url=APefjpFGWA8lZLOL5TetYlTPWvf84OTSo1kBxxNsu1KvWWrMO0tF8suptN-JIJSErom0TSnLZ1pRm2ZLWkrebLlt62WON9hohRwTt3cfDt6Hpo9Unwql4cyVkKM90cWjwoMdZ4FW4t5g&hsutk=&canon=http%3A%2F%2Fwww.californiafamilyfitness.com%2F\"  style=\"/*hs-extra-styles*/\" cta_dest_link=\"http://www.californiafamilyfitness.com/kids\" title=\"Learn more\"> Learn more </a> ","image_html":"<a id=\"cta_button_338227_6bb10ef3-a45e-4994-bcd4-3e74335ec2c7\" class=\"cta_button\" href=\"//cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=6bb10ef3-a45e-4994-bcd4-3e74335ec2c7&placement_guid=d6c5389a-b3a5-4daa-b6c9-ba82ec68e91a&portal_id=338227&redirect_url=APefjpFGWA8lZLOL5TetYlTPWvf84OTSo1kBxxNsu1KvWWrMO0tF8suptN-JIJSErom0TSnLZ1pRm2ZLWkrebLlt62WON9hohRwTt3cfDt6Hpo9Unwql4cyVkKM90cWjwoMdZ4FW4t5g&hsutk=&canon=http%3A%2F%2Fwww.californiafamilyfitness.com%2F\"  cta_dest_link=\"http://www.californiafamilyfitness.com/kids\"> <img id=\"hs-cta-img-d6c5389a-b3a5-4daa-b6c9-ba82ec68e91a\" class=\"hs-cta-img \" style=\"border-width: 0px; /*hs-extra-styles*/\" mce_noresize=\"1\" alt=\"Learn more\" src=\"http://cdn2.hubspot.net/hubshot/17/01/05/5481349d-cb60-46ac-b6f7-9c1ba7f0f43a.png\" /> </a> ","is_image":false,"_class":"hs-cta-d6c5389a-b3a5-4daa-b6c9-ba82ec68e91a"};
    var __hs_cta = {};

    __hs_cta.drop = function() {
        var is_legacy = document.getElementById('hs-cta-ie-element') && true || false,
            html = __hs_cta_json.image_html,
            tags = __hs_cta.getTags(),
            is_image = __hs_cta_json.is_image,
            parent, _style;

        if (!is_legacy && !is_image) {
            parent = (document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]);

            _style = document.createElement('style');
            parent.insertBefore(_style, parent.childNodes[0]);
            try {
                default_css = ".hs-cta-wrapper p, .hs-cta-wrapper div { margin: 0; padding: 0; }";
                cta_css = default_css + " " + __hs_cta_json.css;
                // http://blog.coderlab.us/2005/09/22/using-the-innertext-property-with-firefox/
                _style[document.all ? 'innerText' : 'textContent'] = cta_css;
            } catch (e) {
                // addressing an ie9 issue
                _style.styleSheet.cssText = cta_css;
            }

            html = __hs_cta_json.raw_html;
        }

        for (var i = 0; i < tags.length; ++i) {

            var tag = tags[i];
            var images = tag.getElementsByTagName('img');
            var cssText = "";
            var align = "";
            for (var j = 0; j < images.length; j++) {
                align = images[j].align;
                images[j].style.border = '';
                images[j].style.display = '';
                cssText = images[j].style.cssText;
            }

            if (align == "right") {
                tag.style.display = "block";
                tag.style.textAlign = "right";
            } else if (align == "middle") {
                tag.style.display = "block";
                tag.style.textAlign = "center";
            }

            tag.innerHTML = html.replace('/*hs-extra-styles*/', cssText);
            tag.style.visibility = 'visible';
            tag.setAttribute('data-hs-drop', 'true');
            window.hbspt && hbspt.cta && hbspt.cta.afterLoad && hbspt.cta.afterLoad();
        }

        return tags;
    };

    __hs_cta.getTags = function() {
        var allTags, check, i, divTags, spanTags,
            tags = [];
            if (document.getElementsByClassName) {
                allTags = document.getElementsByClassName(__hs_cta_json._class);
                check = function(ele) {
                    return (ele.nodeName == 'DIV' || ele.nodeName == 'SPAN') && (!ele.getAttribute('data-hs-drop'));
                };
            } else {
                allTags = [];
                divTags = document.getElementsByTagName("div");
                spanTags = document.getElementsByTagName("span");
                for (i = 0; i < spanTags.length; i++) {
                    allTags.push(spanTags[i]);
                }
                for (i = 0; i < divTags.length; i++) {
                    allTags.push(divTags[i]);
                }

                check = function(ele) {
                    return (ele.className.indexOf(__hs_cta_json._class) > -1) && (!ele.getAttribute('data-hs-drop'));
                };
            }
            for (i = 0; i < allTags.length; ++i) {
                if (check(allTags[i])) {
                    tags.push(allTags[i]);
                }
            }
        return tags;
    };

    // need to do a slight timeout so IE has time to react
    setTimeout(__hs_cta.drop, 10);
}());
