(function() {

    var __hs_cta_json = {"css":"a#cta_button_338227_ec32e360-7c5c-4718-88fd-759c43c347aa {\n  cursor:pointer !important; \n}\na#cta_button_338227_ec32e360-7c5c-4718-88fd-759c43c347aa:hover {\n}\na#cta_button_338227_ec32e360-7c5c-4718-88fd-759c43c347aa:active, #cta_button_338227_ec32e360-7c5c-4718-88fd-759c43c347aa:active:hover {\n}\n\n","image":false,"_id":"false","raw_html":"<a id=\"cta_button_338227_ec32e360-7c5c-4718-88fd-759c43c347aa\" class=\"cta_button \" href=\"//cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=ec32e360-7c5c-4718-88fd-759c43c347aa&placement_guid=d111a487-9c6e-499b-92de-4b821f1eea75&portal_id=338227&redirect_url=APefjpFgmBwN1k82VUQLxKcyomHNvXnerJ7d9f9DCJOOHhud8Q8_n3pM9JDhYmvV-BLwwRzFfgIOM9_L3Ax-RQ9Xza7-m8gNtjYYODJY2kFZ4jpH8Ep7xGQoZqj6zOXi1tq1Y-DgoGRo&hsutk=&canon=http%3A%2F%2Fwww.californiafamilyfitness.com%2F\"  style=\"/*hs-extra-styles*/\" cta_dest_link=\"http://www.californiafamilyfitness.com/classes\" title=\"Learn more\"> Learn more </a> ","image_html":"<a id=\"cta_button_338227_ec32e360-7c5c-4718-88fd-759c43c347aa\" class=\"cta_button\" href=\"//cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=ec32e360-7c5c-4718-88fd-759c43c347aa&placement_guid=d111a487-9c6e-499b-92de-4b821f1eea75&portal_id=338227&redirect_url=APefjpFgmBwN1k82VUQLxKcyomHNvXnerJ7d9f9DCJOOHhud8Q8_n3pM9JDhYmvV-BLwwRzFfgIOM9_L3Ax-RQ9Xza7-m8gNtjYYODJY2kFZ4jpH8Ep7xGQoZqj6zOXi1tq1Y-DgoGRo&hsutk=&canon=http%3A%2F%2Fwww.californiafamilyfitness.com%2F\"  cta_dest_link=\"http://www.californiafamilyfitness.com/classes\"> <img id=\"hs-cta-img-d111a487-9c6e-499b-92de-4b821f1eea75\" class=\"hs-cta-img \" style=\"border-width: 0px; /*hs-extra-styles*/\" mce_noresize=\"1\" alt=\"Learn more\" src=\"http://cdn2.hubspot.net/hubshot/17/01/05/348fb416-a334-40a7-bee3-b6b356165ccb.png\" /> </a> ","is_image":false,"_class":"hs-cta-d111a487-9c6e-499b-92de-4b821f1eea75"};
    var __hs_cta = {};

    __hs_cta.drop = function() {
        var is_legacy = document.getElementById('hs-cta-ie-element') && true || false,
            html = __hs_cta_json.image_html,
            tags = __hs_cta.getTags(),
            is_image = __hs_cta_json.is_image,
            parent, _style;

        if (!is_legacy && !is_image) {
            parent = (document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]);

            _style = document.createElement('style');
            parent.insertBefore(_style, parent.childNodes[0]);
            try {
                default_css = ".hs-cta-wrapper p, .hs-cta-wrapper div { margin: 0; padding: 0; }";
                cta_css = default_css + " " + __hs_cta_json.css;
                // http://blog.coderlab.us/2005/09/22/using-the-innertext-property-with-firefox/
                _style[document.all ? 'innerText' : 'textContent'] = cta_css;
            } catch (e) {
                // addressing an ie9 issue
                _style.styleSheet.cssText = cta_css;
            }

            html = __hs_cta_json.raw_html;
        }

        for (var i = 0; i < tags.length; ++i) {

            var tag = tags[i];
            var images = tag.getElementsByTagName('img');
            var cssText = "";
            var align = "";
            for (var j = 0; j < images.length; j++) {
                align = images[j].align;
                images[j].style.border = '';
                images[j].style.display = '';
                cssText = images[j].style.cssText;
            }

            if (align == "right") {
                tag.style.display = "block";
                tag.style.textAlign = "right";
            } else if (align == "middle") {
                tag.style.display = "block";
                tag.style.textAlign = "center";
            }

            tag.innerHTML = html.replace('/*hs-extra-styles*/', cssText);
            tag.style.visibility = 'visible';
            tag.setAttribute('data-hs-drop', 'true');
            window.hbspt && hbspt.cta && hbspt.cta.afterLoad && hbspt.cta.afterLoad();
        }

        return tags;
    };

    __hs_cta.getTags = function() {
        var allTags, check, i, divTags, spanTags,
            tags = [];
            if (document.getElementsByClassName) {
                allTags = document.getElementsByClassName(__hs_cta_json._class);
                check = function(ele) {
                    return (ele.nodeName == 'DIV' || ele.nodeName == 'SPAN') && (!ele.getAttribute('data-hs-drop'));
                };
            } else {
                allTags = [];
                divTags = document.getElementsByTagName("div");
                spanTags = document.getElementsByTagName("span");
                for (i = 0; i < spanTags.length; i++) {
                    allTags.push(spanTags[i]);
                }
                for (i = 0; i < divTags.length; i++) {
                    allTags.push(divTags[i]);
                }

                check = function(ele) {
                    return (ele.className.indexOf(__hs_cta_json._class) > -1) && (!ele.getAttribute('data-hs-drop'));
                };
            }
            for (i = 0; i < allTags.length; ++i) {
                if (check(allTags[i])) {
                    tags.push(allTags[i]);
                }
            }
        return tags;
    };

    // need to do a slight timeout so IE has time to react
    setTimeout(__hs_cta.drop, 10);
}());
